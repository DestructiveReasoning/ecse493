function [k1,k2,k3,k4] = get_acker_ctrl(zeta, wn, fast_poles)
  % zeta:       desired damping coefficient
  % wn:         desired natural frequency
  % fast_poles: vector of two complex numbers describing the third and fourth desired poles
  %             these poles should have very negative real components so they can decay quickly

  statespace; % load state space model (see statespace.m)

  % Poles of second order system: -sigma +/- wd
  sigma = zeta * wn;
  wd = wn*sqrt(zeta^2-1);

  % "Dominant" poles (fast_poles should have more negative real parts)
  p_dom1 = -sigma - wd;
  p_dom2 = -sigma + wd;

  % Determine gains with Ackerman's equation
  K = acker(A,B,[p_dom1,p_dom2,fast_poles(1),fast_poles(2)]);

  % Plot resulting poles and zeros
  sys = ss(A - B*K, B, C, [0;0]);
%  pzmap(sys);
  [ps,zs] = pzmap(sys)
  step(sys)

  k1 = K(1);
  k2 = K(2);
  % Invert angular gains because the basis used to measure angles on the plant
  % is inverted w.r.t the model
  k3 = -K(3);
  k4 = -K(4);
end
