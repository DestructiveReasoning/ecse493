function [k1,k2,k3,k4] = get_lqr(Q, R)
  statespace; % Load state space model (see statespace.m)

  % The following code normalizes the state variables and the input
  normalizer = diag([range_pos^2,range_xvel^2,range_ang^2,range_angvel^2])^(-1);
  Q = normalizer * Q;
  R = R/(range_volts^2); % Scaling R by the range of input voltage

  K = lqr(sys.a,sys.b,Q,R);
  sys = ss(A-B*K,B,C,[0;0]);
  pzmap(sys);
%  step(sys, linspace(0,3));
  k1 = K(1);
  k2 = K(2);
  % Invert the angle gains because plant measures
  % angle in an inverted basis relative to the model
  k3 = -K(3);
  k4 = -K(4);
end
