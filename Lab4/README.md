# Final Lab Assignment for ECSE493 - Control of an Inverted Pendulum

## Requirements

* Stabilize the linearized inverted pendulum by pole placement
* Stabilize the linearized inverted pendulum with an LQR controller

## Directory

This repo contains the Matlab code to model the system, compute controller gains, and record some
results. The `statespace.m` file stores all of the model parameters and computes the state-space
representation of the linearized system, that is, the matrices *A, B, C* such that *ẋ = Ax + Bv* and
*y = Cx*, where *v* is the voltage applied to the motor. By including the line
```Octave
statespace;
```
in a Matlab script, the state-space model is loaded. The controllers in this lab are full-state
controllers, where we have *v = Kx* for some row vector of gains *K*.

The `get_acker_ctrl.m` file generates the gain vector *K* given a desired open-loop damping
coefficient and natural frequency as well as two other, preferably very negative, poles of the
user's choice. The `get_acker_ctrl()` function computes two poles with the damping coefficient and
natural frequency, and uses Ackerman's equation to determine the gains *K*. See `get_acker_ctrl.m`
for more details.

The `get_lqr.m` file generates the gain vector *K* with the Linear Quadratic Regulator method (LQR),
given weight matrices $Q$ and $R$. See `get_lqr.m` for more details.

The `ackerconfigs.txt` and `lqrconfigs.txt` files list some of the tested configurations for both
the pole placement and LQR controllers, respectively. The files describe the behavior observed for
each configuration, and suggest the optimal configuration that was observed.
