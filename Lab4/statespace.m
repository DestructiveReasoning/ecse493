mp = 0.23;    % weight of the pendulum
lp = 2*0.168; % since we use the point-mass approximation, lp becomes total length of rod
mc = 2*0.526; % approximate weight of the cart, 0.526kg is too low
g = 9.81;
Km = 0.0077;
Kg = 3.7;
Ra = 2.6;
r = 0.0064;

range_pos = 1;          %track length of about 1m
range_xvel = 2;         %limit to 2m/s
range_ang = 20*pi/180;  %+/- 10 degrees
range_angvel = pi/2;    %limit to 60deg/s
range_volts = 10;       %voltage in [-5,5]V

A = [   0   1   0   0;
       0   -(Km^2*Kg^2/(Ra*r^2*mc))   -(mp*g/mc)  0;
       0   0   0   1;
       0   Km^2*Kg^2/(Ra*r^2*mc*lp)   (mc+mp)*g/(mc*lp)    0];
B = [0; Kg*Km/(Ra*r*mc); 0; -Kg*Km/(Ra*r*mc*lp)];
C = [1 0 0 0; 0 0 1 0];
sys = ss(A,B,C);
