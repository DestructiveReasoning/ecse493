function [k1,k2,k3,k4] = get_acker_ctrl(zeta, wn, fast_poles)
    statespace;
    sigma = zeta * wn;
    wd = wn*sqrt(1-zeta^2);
    p_dom1 = -sigma - wd;
    p_dom2 = -sigma + wd;
    K = acker(A,B,[p_dom1,p_dom2,fast_poles(1),fast_poles(2)]);
    sys = ss(A - B*K, [0;0;0;0], C, [0;0]);
    pzmap(sys);
    k1 = K(1);
    k2 = K(2);
    k3 = -K(3);
    k4 = -K(4);
end
    
    