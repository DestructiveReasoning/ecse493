function [k1,k2,k3,k4] = get_lqr(Q, R)
    statespace;
    Ai = [ 0 0 0 0;
           0 Kg^2*Km^2*mc*(lp/2/(r^2*Ra*(mc*I + mp*I + mp*mc*(lp/2)^2))) 1 -(lp/2)^2*mp*Kg^2*Km^2-I*Km^2*Kg^2/(r^2*Ra*(mc*I+mp*I+mp*mc*(lp/2)^2));
           0 (mc+mp)*g*mp*(lp/2) / ((mc+mp)*I + mp*mc*(lp/2)^2) 0 -g*mp^2*(lp/2)^2/((mc+mp)*I + mp*mc*(lp/2)^2);
           1 0 0 0].';
    Bi = [0;
          (mp*(lp/2)^2*Kg*Km*r + Kg*Km*I*r)/(r*Ra*(mc*I + mp*I + mc*mp*(lp/2)^2));
          0;
          -Kg*Km*mp*(lp/2)/(r*Ra*(mc*I + mp*I + mc*mp*(lp/2)^2))];
    
    normalizer = diag([range_pos^2,range_xvel^2,range_ang^2,range_angvel^2])^(-1);
    %Q = normalizer * Q;
    %R = R/(range_volts^2);
    
    K = lqr(A,B,Q,R,0);
    [ps,zs] = pzmap(A-B*K,B,C,[0;0])
    sys = ss(A-B*K,B,C,[0;0]);
    step(sys)
    k1 = K(1);
    k2 = K(2);
    % Invert the angle gains because plant measures
    % angle in an inverted basis relative to the model
    k3 = -K(3);
    k4 = -K(4);
end