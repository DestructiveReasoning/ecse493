  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 4;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (lab4_P)
    ;%
      section.nData     = 38;
      section.data(38)  = dumData; %prealloc
      
	  ;% lab4_P.Constant1_Value
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% lab4_P.Constant_Value
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% lab4_P.HILInitialize_OOStart
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% lab4_P.HILInitialize_OOEnter
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% lab4_P.HILInitialize_OOTerminate
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% lab4_P.HILInitialize_OOExit
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% lab4_P.HILInitialize_AIHigh
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% lab4_P.HILInitialize_AILow
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% lab4_P.HILInitialize_AOHigh
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
	  ;% lab4_P.HILInitialize_AOLow
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 9;
	
	  ;% lab4_P.HILInitialize_AOInitial
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 10;
	
	  ;% lab4_P.HILInitialize_AOFinal
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 11;
	
	  ;% lab4_P.HILInitialize_AOWatchdog
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 12;
	
	  ;% lab4_P.HILInitialize_EIFrequency
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 13;
	
	  ;% lab4_P.HILInitialize_POFrequency
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 14;
	
	  ;% lab4_P.HILInitialize_POInitial
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 15;
	
	  ;% lab4_P.HILInitialize_POFinal
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 16;
	
	  ;% lab4_P.Gain4_Gain
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 17;
	
	  ;% lab4_P.TSamp_WtEt
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 18;
	
	  ;% lab4_P.UD_X0
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 19;
	
	  ;% lab4_P.GeneratedFilterBlock_RTP1COEFF
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 20;
	
	  ;% lab4_P.Gain_Gain
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 42;
	
	  ;% lab4_P.Gain1_Gain
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 43;
	
	  ;% lab4_P.Gain5_Gain
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 44;
	
	  ;% lab4_P.Gain2_Gain
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 45;
	
	  ;% lab4_P.TSamp_WtEt_i
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 46;
	
	  ;% lab4_P.UD_X0_a
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 47;
	
	  ;% lab4_P.GeneratedFilterBlock_RTP1COEF_m
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 48;
	
	  ;% lab4_P.Gain3_Gain
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 65;
	
	  ;% lab4_P.Gain6_Gain
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 66;
	
	  ;% lab4_P.DiscreteTimeIntegrator_gainval
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 67;
	
	  ;% lab4_P.DiscreteTimeIntegrator_IC
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 68;
	
	  ;% lab4_P.Gain7_Gain
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 69;
	
	  ;% lab4_P.DiscreteTimeIntegrator1_gainval
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 70;
	
	  ;% lab4_P.DiscreteTimeIntegrator1_IC
	  section.data(35).logicalSrcIdx = 34;
	  section.data(35).dtTransOffset = 71;
	
	  ;% lab4_P.Gain8_Gain
	  section.data(36).logicalSrcIdx = 35;
	  section.data(36).dtTransOffset = 72;
	
	  ;% lab4_P.Gain9_Gain
	  section.data(37).logicalSrcIdx = 36;
	  section.data(37).dtTransOffset = 73;
	
	  ;% lab4_P.Switch_Threshold
	  section.data(38).logicalSrcIdx = 37;
	  section.data(38).dtTransOffset = 74;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% lab4_P.HILInitialize_CKChannels
	  section.data(1).logicalSrcIdx = 38;
	  section.data(1).dtTransOffset = 0;
	
	  ;% lab4_P.HILInitialize_CKModes
	  section.data(2).logicalSrcIdx = 39;
	  section.data(2).dtTransOffset = 2;
	
	  ;% lab4_P.HILInitialize_DOWatchdog
	  section.data(3).logicalSrcIdx = 40;
	  section.data(3).dtTransOffset = 4;
	
	  ;% lab4_P.HILInitialize_EIInitial
	  section.data(4).logicalSrcIdx = 41;
	  section.data(4).dtTransOffset = 5;
	
	  ;% lab4_P.HILInitialize_POModes
	  section.data(5).logicalSrcIdx = 42;
	  section.data(5).dtTransOffset = 6;
	
	  ;% lab4_P.HILReadEncoderTimebase_Clock
	  section.data(6).logicalSrcIdx = 43;
	  section.data(6).dtTransOffset = 7;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 7;
      section.data(7)  = dumData; %prealloc
      
	  ;% lab4_P.HILInitialize_AIChannels
	  section.data(1).logicalSrcIdx = 44;
	  section.data(1).dtTransOffset = 0;
	
	  ;% lab4_P.HILInitialize_AOChannels
	  section.data(2).logicalSrcIdx = 45;
	  section.data(2).dtTransOffset = 4;
	
	  ;% lab4_P.HILInitialize_EIChannels
	  section.data(3).logicalSrcIdx = 46;
	  section.data(3).dtTransOffset = 8;
	
	  ;% lab4_P.HILInitialize_EIQuadrature
	  section.data(4).logicalSrcIdx = 47;
	  section.data(4).dtTransOffset = 12;
	
	  ;% lab4_P.HILReadEncoderTimebase_Channels
	  section.data(5).logicalSrcIdx = 48;
	  section.data(5).dtTransOffset = 13;
	
	  ;% lab4_P.HILReadEncoderTimebase_SamplesI
	  section.data(6).logicalSrcIdx = 49;
	  section.data(6).dtTransOffset = 15;
	
	  ;% lab4_P.HILWriteAnalog_Channels
	  section.data(7).logicalSrcIdx = 50;
	  section.data(7).dtTransOffset = 16;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
      section.nData     = 37;
      section.data(37)  = dumData; %prealloc
      
	  ;% lab4_P.HILInitialize_Active
	  section.data(1).logicalSrcIdx = 51;
	  section.data(1).dtTransOffset = 0;
	
	  ;% lab4_P.HILInitialize_CKPStart
	  section.data(2).logicalSrcIdx = 52;
	  section.data(2).dtTransOffset = 1;
	
	  ;% lab4_P.HILInitialize_CKPEnter
	  section.data(3).logicalSrcIdx = 53;
	  section.data(3).dtTransOffset = 2;
	
	  ;% lab4_P.HILInitialize_CKStart
	  section.data(4).logicalSrcIdx = 54;
	  section.data(4).dtTransOffset = 3;
	
	  ;% lab4_P.HILInitialize_CKEnter
	  section.data(5).logicalSrcIdx = 55;
	  section.data(5).dtTransOffset = 4;
	
	  ;% lab4_P.HILInitialize_AIPStart
	  section.data(6).logicalSrcIdx = 56;
	  section.data(6).dtTransOffset = 5;
	
	  ;% lab4_P.HILInitialize_AIPEnter
	  section.data(7).logicalSrcIdx = 57;
	  section.data(7).dtTransOffset = 6;
	
	  ;% lab4_P.HILInitialize_AOPStart
	  section.data(8).logicalSrcIdx = 58;
	  section.data(8).dtTransOffset = 7;
	
	  ;% lab4_P.HILInitialize_AOPEnter
	  section.data(9).logicalSrcIdx = 59;
	  section.data(9).dtTransOffset = 8;
	
	  ;% lab4_P.HILInitialize_AOStart
	  section.data(10).logicalSrcIdx = 60;
	  section.data(10).dtTransOffset = 9;
	
	  ;% lab4_P.HILInitialize_AOEnter
	  section.data(11).logicalSrcIdx = 61;
	  section.data(11).dtTransOffset = 10;
	
	  ;% lab4_P.HILInitialize_AOTerminate
	  section.data(12).logicalSrcIdx = 62;
	  section.data(12).dtTransOffset = 11;
	
	  ;% lab4_P.HILInitialize_AOExit
	  section.data(13).logicalSrcIdx = 63;
	  section.data(13).dtTransOffset = 12;
	
	  ;% lab4_P.HILInitialize_AOReset
	  section.data(14).logicalSrcIdx = 64;
	  section.data(14).dtTransOffset = 13;
	
	  ;% lab4_P.HILInitialize_DOPStart
	  section.data(15).logicalSrcIdx = 65;
	  section.data(15).dtTransOffset = 14;
	
	  ;% lab4_P.HILInitialize_DOPEnter
	  section.data(16).logicalSrcIdx = 66;
	  section.data(16).dtTransOffset = 15;
	
	  ;% lab4_P.HILInitialize_DOStart
	  section.data(17).logicalSrcIdx = 67;
	  section.data(17).dtTransOffset = 16;
	
	  ;% lab4_P.HILInitialize_DOEnter
	  section.data(18).logicalSrcIdx = 68;
	  section.data(18).dtTransOffset = 17;
	
	  ;% lab4_P.HILInitialize_DOTerminate
	  section.data(19).logicalSrcIdx = 69;
	  section.data(19).dtTransOffset = 18;
	
	  ;% lab4_P.HILInitialize_DOExit
	  section.data(20).logicalSrcIdx = 70;
	  section.data(20).dtTransOffset = 19;
	
	  ;% lab4_P.HILInitialize_DOReset
	  section.data(21).logicalSrcIdx = 71;
	  section.data(21).dtTransOffset = 20;
	
	  ;% lab4_P.HILInitialize_EIPStart
	  section.data(22).logicalSrcIdx = 72;
	  section.data(22).dtTransOffset = 21;
	
	  ;% lab4_P.HILInitialize_EIPEnter
	  section.data(23).logicalSrcIdx = 73;
	  section.data(23).dtTransOffset = 22;
	
	  ;% lab4_P.HILInitialize_EIStart
	  section.data(24).logicalSrcIdx = 74;
	  section.data(24).dtTransOffset = 23;
	
	  ;% lab4_P.HILInitialize_EIEnter
	  section.data(25).logicalSrcIdx = 75;
	  section.data(25).dtTransOffset = 24;
	
	  ;% lab4_P.HILInitialize_POPStart
	  section.data(26).logicalSrcIdx = 76;
	  section.data(26).dtTransOffset = 25;
	
	  ;% lab4_P.HILInitialize_POPEnter
	  section.data(27).logicalSrcIdx = 77;
	  section.data(27).dtTransOffset = 26;
	
	  ;% lab4_P.HILInitialize_POStart
	  section.data(28).logicalSrcIdx = 78;
	  section.data(28).dtTransOffset = 27;
	
	  ;% lab4_P.HILInitialize_POEnter
	  section.data(29).logicalSrcIdx = 79;
	  section.data(29).dtTransOffset = 28;
	
	  ;% lab4_P.HILInitialize_POTerminate
	  section.data(30).logicalSrcIdx = 80;
	  section.data(30).dtTransOffset = 29;
	
	  ;% lab4_P.HILInitialize_POExit
	  section.data(31).logicalSrcIdx = 81;
	  section.data(31).dtTransOffset = 30;
	
	  ;% lab4_P.HILInitialize_POReset
	  section.data(32).logicalSrcIdx = 82;
	  section.data(32).dtTransOffset = 31;
	
	  ;% lab4_P.HILInitialize_OOReset
	  section.data(33).logicalSrcIdx = 83;
	  section.data(33).dtTransOffset = 32;
	
	  ;% lab4_P.HILInitialize_DOInitial
	  section.data(34).logicalSrcIdx = 84;
	  section.data(34).dtTransOffset = 33;
	
	  ;% lab4_P.HILInitialize_DOFinal
	  section.data(35).logicalSrcIdx = 85;
	  section.data(35).dtTransOffset = 34;
	
	  ;% lab4_P.HILReadEncoderTimebase_Active
	  section.data(36).logicalSrcIdx = 86;
	  section.data(36).dtTransOffset = 35;
	
	  ;% lab4_P.HILWriteAnalog_Active
	  section.data(37).logicalSrcIdx = 87;
	  section.data(37).dtTransOffset = 36;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(4) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 1;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (lab4_B)
    ;%
      section.nData     = 9;
      section.data(9)  = dumData; %prealloc
      
	  ;% lab4_B.Gain4
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% lab4_B.TSamp
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% lab4_B.GeneratedFilterBlock
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% lab4_B.Gain5
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 3;
	
	  ;% lab4_B.TSamp_e
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 4;
	
	  ;% lab4_B.GeneratedFilterBlock_j
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 5;
	
	  ;% lab4_B.Sum3
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 6;
	
	  ;% lab4_B.Switch
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 7;
	
	  ;% lab4_B.MinMax
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 8;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 5;
    sectIdxOffset = 1;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (lab4_DWork)
    ;%
      section.nData     = 12;
      section.data(12)  = dumData; %prealloc
      
	  ;% lab4_DWork.UD_DSTATE
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% lab4_DWork.GeneratedFilterBlock_FILT_STATE
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% lab4_DWork.UD_DSTATE_b
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 23;
	
	  ;% lab4_DWork.GeneratedFilterBlock_FILT_STA_g
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 24;
	
	  ;% lab4_DWork.DiscreteTimeIntegrator_DSTATE
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 41;
	
	  ;% lab4_DWork.DiscreteTimeIntegrator1_DSTATE
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 42;
	
	  ;% lab4_DWork.HILInitialize_AIMinimums
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 43;
	
	  ;% lab4_DWork.HILInitialize_AIMaximums
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 47;
	
	  ;% lab4_DWork.HILInitialize_AOMinimums
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 51;
	
	  ;% lab4_DWork.HILInitialize_AOMaximums
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 55;
	
	  ;% lab4_DWork.HILInitialize_AOVoltages
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 59;
	
	  ;% lab4_DWork.HILInitialize_FilterFrequency
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 63;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% lab4_DWork.HILInitialize_Card
	  section.data(1).logicalSrcIdx = 12;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% lab4_DWork.HILReadEncoderTimebase_Task
	  section.data(1).logicalSrcIdx = 13;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 8;
      section.data(8)  = dumData; %prealloc
      
	  ;% lab4_DWork.HILWriteAnalog_PWORK
	  section.data(1).logicalSrcIdx = 14;
	  section.data(1).dtTransOffset = 0;
	
	  ;% lab4_DWork.ToWorkspace_PWORK.LoggedData
	  section.data(2).logicalSrcIdx = 15;
	  section.data(2).dtTransOffset = 1;
	
	  ;% lab4_DWork.ToWorkspace1_PWORK.LoggedData
	  section.data(3).logicalSrcIdx = 16;
	  section.data(3).dtTransOffset = 2;
	
	  ;% lab4_DWork.ToWorkspace2_PWORK.LoggedData
	  section.data(4).logicalSrcIdx = 17;
	  section.data(4).dtTransOffset = 3;
	
	  ;% lab4_DWork.ToWorkspace3_PWORK.LoggedData
	  section.data(5).logicalSrcIdx = 18;
	  section.data(5).dtTransOffset = 4;
	
	  ;% lab4_DWork.ToWorkspace4_PWORK.LoggedData
	  section.data(6).logicalSrcIdx = 19;
	  section.data(6).dtTransOffset = 5;
	
	  ;% lab4_DWork.ToWorkspace5_PWORK.LoggedData
	  section.data(7).logicalSrcIdx = 20;
	  section.data(7).dtTransOffset = 6;
	
	  ;% lab4_DWork.ToWorkspace6_PWORK.LoggedData
	  section.data(8).logicalSrcIdx = 21;
	  section.data(8).dtTransOffset = 7;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
      section.nData     = 5;
      section.data(5)  = dumData; %prealloc
      
	  ;% lab4_DWork.HILInitialize_QuadratureModes
	  section.data(1).logicalSrcIdx = 22;
	  section.data(1).dtTransOffset = 0;
	
	  ;% lab4_DWork.HILInitialize_InitialEICounts
	  section.data(2).logicalSrcIdx = 23;
	  section.data(2).dtTransOffset = 4;
	
	  ;% lab4_DWork.HILReadEncoderTimebase_Buffer
	  section.data(3).logicalSrcIdx = 24;
	  section.data(3).dtTransOffset = 8;
	
	  ;% lab4_DWork.GeneratedFilterBlock_CIRCBUFFID
	  section.data(4).logicalSrcIdx = 25;
	  section.data(4).dtTransOffset = 10;
	
	  ;% lab4_DWork.GeneratedFilterBlock_CIRCBUFF_c
	  section.data(5).logicalSrcIdx = 26;
	  section.data(5).dtTransOffset = 11;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(5) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 4224482832;
  targMap.checksum1 = 2361382022;
  targMap.checksum2 = 2483723626;
  targMap.checksum3 = 630661412;

