/*
 * lab4.c
 *
 * Code generation for model "lab4.mdl".
 *
 * Model version              : 1.529
 * Simulink Coder version : 8.1 (R2011b) 08-Jul-2011
 * C source code generated on : Mon Dec 10 14:03:45 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "lab4.h"
#include "lab4_private.h"
#include "lab4_dt.h"

/* Block signals (auto storage) */
BlockIO_lab4 lab4_B;

/* Block states (auto storage) */
D_Work_lab4 lab4_DWork;

/* Real-time model */
RT_MODEL_lab4 lab4_M_;
RT_MODEL_lab4 *const lab4_M = &lab4_M_;

/* Model output function */
void lab4_output(int_T tid)
{
  /* local block i/o variables */
  real_T rtb_HILReadEncoderTimebase_o1;
  real_T rtb_Abs;
  int32_T cfIdx;
  int32_T j;
  real_T rtb_Gain7;

  /* S-Function (hil_read_encoder_timebase_block): '<Root>/HIL Read Encoder Timebase' */

  /* S-Function Block: lab4/HIL Read Encoder Timebase (hil_read_encoder_timebase_block) */
  {
    t_error result;
    result = hil_task_read_encoder(lab4_DWork.HILReadEncoderTimebase_Task, 1,
      &lab4_DWork.HILReadEncoderTimebase_Buffer[0]);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab4_M, _rt_error_message);
    } else {
      rtb_HILReadEncoderTimebase_o1 = lab4_DWork.HILReadEncoderTimebase_Buffer[0];
      rtb_Abs = lab4_DWork.HILReadEncoderTimebase_Buffer[1];
    }
  }

  /* Gain: '<Root>/Gain4' */
  lab4_B.Gain4 = lab4_P.Gain4_Gain * rtb_HILReadEncoderTimebase_o1;

  /* SampleTimeMath: '<S1>/TSamp'
   *
   * About '<S1>/TSamp':
   *  y = u * K where K = 1 / ( w * Ts )
   */
  lab4_B.TSamp = lab4_B.Gain4 * lab4_P.TSamp_WtEt;

  /* Sum: '<S1>/Diff' incorporates:
   *  UnitDelay: '<S1>/UD'
   */
  lab4_B.Diff = lab4_B.TSamp - lab4_DWork.UD_DSTATE;

  /* S-Function (sdspfilter2): '<S4>/Generated Filter Block' */
  rtb_Gain7 = lab4_B.Diff * lab4_P.GeneratedFilterBlock_RTP1COEFF[0];
  cfIdx = 1;
  for (j = lab4_DWork.GeneratedFilterBlock_CIRCBUFFID; j < 21; j++) {
    rtb_Gain7 += lab4_DWork.GeneratedFilterBlock_FILT_STATE[j] *
      lab4_P.GeneratedFilterBlock_RTP1COEFF[cfIdx];
    cfIdx++;
  }

  for (j = 0; j < lab4_DWork.GeneratedFilterBlock_CIRCBUFFID; j++) {
    rtb_Gain7 += lab4_DWork.GeneratedFilterBlock_FILT_STATE[j] *
      lab4_P.GeneratedFilterBlock_RTP1COEFF[cfIdx];
    cfIdx++;
  }

  cfIdx = lab4_DWork.GeneratedFilterBlock_CIRCBUFFID - 1;
  if (cfIdx < 0) {
    cfIdx = 20;
  }

  lab4_DWork.GeneratedFilterBlock_FILT_STATE[cfIdx] = lab4_B.Diff;
  lab4_B.GeneratedFilterBlock = rtb_Gain7;
  lab4_DWork.GeneratedFilterBlock_CIRCBUFFID = cfIdx;

  /* End of S-Function (sdspfilter2): '<S4>/Generated Filter Block' */

  /* Gain: '<Root>/Gain5' */
  lab4_B.Gain5 = lab4_P.Gain5_Gain * rtb_Abs;

  /* SampleTimeMath: '<S2>/TSamp'
   *
   * About '<S2>/TSamp':
   *  y = u * K where K = 1 / ( w * Ts )
   */
  lab4_B.TSamp_e = lab4_B.Gain5 * lab4_P.TSamp_WtEt_i;

  /* Sum: '<S2>/Diff' incorporates:
   *  UnitDelay: '<S2>/UD'
   */
  lab4_B.Diff = lab4_B.TSamp_e - lab4_DWork.UD_DSTATE_b;

  /* S-Function (sdspfilter2): '<S3>/Generated Filter Block' */
  rtb_Gain7 = lab4_B.Diff * lab4_P.GeneratedFilterBlock_RTP1COEF_m[0];
  cfIdx = 1;
  for (j = lab4_DWork.GeneratedFilterBlock_CIRCBUFF_c; j < 16; j++) {
    rtb_Gain7 += lab4_DWork.GeneratedFilterBlock_FILT_STA_g[j] *
      lab4_P.GeneratedFilterBlock_RTP1COEF_m[cfIdx];
    cfIdx++;
  }

  for (j = 0; j < lab4_DWork.GeneratedFilterBlock_CIRCBUFF_c; j++) {
    rtb_Gain7 += lab4_DWork.GeneratedFilterBlock_FILT_STA_g[j] *
      lab4_P.GeneratedFilterBlock_RTP1COEF_m[cfIdx];
    cfIdx++;
  }

  cfIdx = lab4_DWork.GeneratedFilterBlock_CIRCBUFF_c - 1;
  if (cfIdx < 0) {
    cfIdx = 15;
  }

  lab4_DWork.GeneratedFilterBlock_FILT_STA_g[cfIdx] = lab4_B.Diff;
  lab4_B.GeneratedFilterBlock_j = rtb_Gain7;
  lab4_DWork.GeneratedFilterBlock_CIRCBUFF_c = cfIdx;

  /* End of S-Function (sdspfilter2): '<S3>/Generated Filter Block' */

  /* DiscreteIntegrator: '<Root>/Discrete-Time Integrator' */
  rtb_Abs = lab4_DWork.DiscreteTimeIntegrator_DSTATE;

  /* Gain: '<Root>/Gain7' */
  rtb_Gain7 = lab4_P.Gain7_Gain * rtb_Abs;

  /* DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' */
  rtb_Abs = lab4_DWork.DiscreteTimeIntegrator1_DSTATE;

  /* Sum: '<Root>/Sum3' incorporates:
   *  Gain: '<Root>/Gain'
   *  Gain: '<Root>/Gain1'
   *  Gain: '<Root>/Gain2'
   *  Gain: '<Root>/Gain3'
   *  Gain: '<Root>/Gain6'
   *  Gain: '<Root>/Gain8'
   *  Gain: '<Root>/Gain9'
   *  Sum: '<Root>/Sum'
   *  Sum: '<Root>/Sum1'
   *  Sum: '<Root>/Sum2'
   *  Sum: '<Root>/Sum4'
   */
  lab4_B.Sum3 = ((lab4_P.Gain_Gain * lab4_B.GeneratedFilterBlock +
                  lab4_P.Gain1_Gain * lab4_B.Gain4) + (lab4_P.Gain2_Gain *
    lab4_B.Gain5 + lab4_P.Gain3_Gain * lab4_B.GeneratedFilterBlock_j)) *
    lab4_P.Gain6_Gain + (lab4_P.Gain8_Gain * rtb_Abs + rtb_Gain7) *
    lab4_P.Gain9_Gain;

  /* S-Function (hil_write_analog_block): '<Root>/HIL Write Analog' */

  /* S-Function Block: lab4/HIL Write Analog (hil_write_analog_block) */
  {
    t_error result;
    result = hil_write_analog(lab4_DWork.HILInitialize_Card,
      &lab4_P.HILWriteAnalog_Channels, 1, &lab4_B.Sum3);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab4_M, _rt_error_message);
    }
  }

  /* Abs: '<Root>/Abs' */
  rtb_Abs = fabs(lab4_B.Gain5);

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant'
   *  Constant: '<Root>/Constant1'
   */
  if (rtb_Abs >= lab4_P.Switch_Threshold) {
    lab4_B.Switch = lab4_P.Constant_Value;
  } else {
    lab4_B.Switch = lab4_P.Constant1_Value;
  }

  /* End of Switch: '<Root>/Switch' */

  /* Stop: '<Root>/Stop Simulation' */
  if (lab4_B.Switch != 0.0) {
    rtmSetStopRequested(lab4_M, 1);
  }

  /* End of Stop: '<Root>/Stop Simulation' */

  /* MinMax: '<Root>/MinMax' */
  lab4_B.MinMax = rtb_Abs;

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model update function */
void lab4_update(int_T tid)
{
  /* Update for UnitDelay: '<S1>/UD' */
  lab4_DWork.UD_DSTATE = lab4_B.TSamp;

  /* Update for UnitDelay: '<S2>/UD' */
  lab4_DWork.UD_DSTATE_b = lab4_B.TSamp_e;

  /* Update for DiscreteIntegrator: '<Root>/Discrete-Time Integrator' */
  lab4_DWork.DiscreteTimeIntegrator_DSTATE =
    lab4_P.DiscreteTimeIntegrator_gainval * lab4_B.Gain4 +
    lab4_DWork.DiscreteTimeIntegrator_DSTATE;

  /* Update for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' */
  lab4_DWork.DiscreteTimeIntegrator1_DSTATE =
    lab4_P.DiscreteTimeIntegrator1_gainval * lab4_B.Gain5 +
    lab4_DWork.DiscreteTimeIntegrator1_DSTATE;

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++lab4_M->Timing.clockTick0)) {
    ++lab4_M->Timing.clockTickH0;
  }

  lab4_M->Timing.t[0] = lab4_M->Timing.clockTick0 * lab4_M->Timing.stepSize0 +
    lab4_M->Timing.clockTickH0 * lab4_M->Timing.stepSize0 * 4294967296.0;

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model initialize function */
void lab4_initialize(boolean_T firstTime)
{
  (void)firstTime;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)lab4_M, 0,
                sizeof(RT_MODEL_lab4));

  /* Initialize timing info */
  {
    int_T *mdlTsMap = lab4_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    lab4_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    lab4_M->Timing.sampleTimes = (&lab4_M->Timing.sampleTimesArray[0]);
    lab4_M->Timing.offsetTimes = (&lab4_M->Timing.offsetTimesArray[0]);

    /* task periods */
    lab4_M->Timing.sampleTimes[0] = (0.002);

    /* task offsets */
    lab4_M->Timing.offsetTimes[0] = (0.0);
  }

  rtmSetTPtr(lab4_M, &lab4_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = lab4_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    lab4_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(lab4_M, -1);
  lab4_M->Timing.stepSize0 = 0.002;

  /* external mode info */
  lab4_M->Sizes.checksums[0] = (4224482832U);
  lab4_M->Sizes.checksums[1] = (2361382022U);
  lab4_M->Sizes.checksums[2] = (2483723626U);
  lab4_M->Sizes.checksums[3] = (630661412U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[3];
    lab4_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    systemRan[1] = &rtAlwaysEnabled;
    systemRan[2] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(lab4_M->extModeInfo,
      &lab4_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(lab4_M->extModeInfo, lab4_M->Sizes.checksums);
    rteiSetTPtr(lab4_M->extModeInfo, rtmGetTPtr(lab4_M));
  }

  lab4_M->solverInfoPtr = (&lab4_M->solverInfo);
  lab4_M->Timing.stepSize = (0.002);
  rtsiSetFixedStepSize(&lab4_M->solverInfo, 0.002);
  rtsiSetSolverMode(&lab4_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  lab4_M->ModelData.blockIO = ((void *) &lab4_B);
  (void) memset(((void *) &lab4_B), 0,
                sizeof(BlockIO_lab4));

  /* parameters */
  lab4_M->ModelData.defaultParam = ((real_T *)&lab4_P);

  /* states (dwork) */
  lab4_M->Work.dwork = ((void *) &lab4_DWork);
  (void) memset((void *)&lab4_DWork, 0,
                sizeof(D_Work_lab4));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    lab4_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }
}

/* Model terminate function */
void lab4_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: lab4/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(lab4_DWork.HILInitialize_Card);
    hil_monitor_stop_all(lab4_DWork.HILInitialize_Card);
    is_switching = false;
    if ((lab4_P.HILInitialize_AOTerminate && !is_switching) ||
        (lab4_P.HILInitialize_AOExit && is_switching)) {
      lab4_DWork.HILInitialize_AOVoltages[0] = lab4_P.HILInitialize_AOFinal;
      lab4_DWork.HILInitialize_AOVoltages[1] = lab4_P.HILInitialize_AOFinal;
      lab4_DWork.HILInitialize_AOVoltages[2] = lab4_P.HILInitialize_AOFinal;
      lab4_DWork.HILInitialize_AOVoltages[3] = lab4_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 4U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(lab4_DWork.HILInitialize_Card,
        lab4_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &lab4_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab4_M, _rt_error_message);
      }
    }

    hil_task_delete_all(lab4_DWork.HILInitialize_Card);
    hil_monitor_delete_all(lab4_DWork.HILInitialize_Card);
    hil_close(lab4_DWork.HILInitialize_Card);
    lab4_DWork.HILInitialize_Card = NULL;
  }
}

/*========================================================================*
 * Start of GRT compatible call interface                                 *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  lab4_output(tid);
}

void MdlUpdate(int_T tid)
{
  lab4_update(tid);
}

void MdlInitializeSizes(void)
{
  lab4_M->Sizes.numContStates = (0);   /* Number of continuous states */
  lab4_M->Sizes.numY = (0);            /* Number of model outputs */
  lab4_M->Sizes.numU = (0);            /* Number of model inputs */
  lab4_M->Sizes.sysDirFeedThru = (0);  /* The model is not direct feedthrough */
  lab4_M->Sizes.numSampTimes = (1);    /* Number of sample times */
  lab4_M->Sizes.numBlocks = (43);      /* Number of blocks */
  lab4_M->Sizes.numBlockIO = (10);     /* Number of block outputs */
  lab4_M->Sizes.numBlockPrms = (137);  /* Sum of parameter "widths" */
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
  /* InitializeConditions for UnitDelay: '<S1>/UD' */
  lab4_DWork.UD_DSTATE = lab4_P.UD_X0;

  /* InitializeConditions for S-Function (sdspfilter2): '<S4>/Generated Filter Block' */
  lab4_DWork.GeneratedFilterBlock_CIRCBUFFID = 0;
  memset((void *)lab4_DWork.GeneratedFilterBlock_FILT_STATE, 0, 21U * sizeof
         (real_T));
  lab4_DWork.GeneratedFilterBlock_FILT_STATE[21] = 0.0;

  /* InitializeConditions for UnitDelay: '<S2>/UD' */
  lab4_DWork.UD_DSTATE_b = lab4_P.UD_X0_a;

  /* InitializeConditions for S-Function (sdspfilter2): '<S3>/Generated Filter Block' */
  lab4_DWork.GeneratedFilterBlock_CIRCBUFF_c = 0;
  memset((void *)lab4_DWork.GeneratedFilterBlock_FILT_STA_g, 0, sizeof(real_T) <<
         4U);
  lab4_DWork.GeneratedFilterBlock_FILT_STA_g[16] = 0.0;

  /* InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator' */
  lab4_DWork.DiscreteTimeIntegrator_DSTATE = lab4_P.DiscreteTimeIntegrator_IC;

  /* InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator1' */
  lab4_DWork.DiscreteTimeIntegrator1_DSTATE = lab4_P.DiscreteTimeIntegrator1_IC;
}

void MdlStart(void)
{
  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: lab4/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q4", "0", &lab4_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab4_M, _rt_error_message);
      return;
    }

    is_switching = false;
    if ((lab4_P.HILInitialize_CKPStart && !is_switching) ||
        (lab4_P.HILInitialize_CKPEnter && is_switching)) {
      result = hil_set_clock_mode(lab4_DWork.HILInitialize_Card, (t_clock *)
        lab4_P.HILInitialize_CKChannels, 2U, (t_clock_mode *)
        lab4_P.HILInitialize_CKModes);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab4_M, _rt_error_message);
        return;
      }
    }

    result = hil_watchdog_clear(lab4_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab4_M, _rt_error_message);
      return;
    }

    if ((lab4_P.HILInitialize_AIPStart && !is_switching) ||
        (lab4_P.HILInitialize_AIPEnter && is_switching)) {
      lab4_DWork.HILInitialize_AIMinimums[0] = lab4_P.HILInitialize_AILow;
      lab4_DWork.HILInitialize_AIMinimums[1] = lab4_P.HILInitialize_AILow;
      lab4_DWork.HILInitialize_AIMinimums[2] = lab4_P.HILInitialize_AILow;
      lab4_DWork.HILInitialize_AIMinimums[3] = lab4_P.HILInitialize_AILow;
      lab4_DWork.HILInitialize_AIMaximums[0] = lab4_P.HILInitialize_AIHigh;
      lab4_DWork.HILInitialize_AIMaximums[1] = lab4_P.HILInitialize_AIHigh;
      lab4_DWork.HILInitialize_AIMaximums[2] = lab4_P.HILInitialize_AIHigh;
      lab4_DWork.HILInitialize_AIMaximums[3] = lab4_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(lab4_DWork.HILInitialize_Card,
        lab4_P.HILInitialize_AIChannels, 4U,
        &lab4_DWork.HILInitialize_AIMinimums[0],
        &lab4_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab4_M, _rt_error_message);
        return;
      }
    }

    if ((lab4_P.HILInitialize_AOPStart && !is_switching) ||
        (lab4_P.HILInitialize_AOPEnter && is_switching)) {
      lab4_DWork.HILInitialize_AOMinimums[0] = lab4_P.HILInitialize_AOLow;
      lab4_DWork.HILInitialize_AOMinimums[1] = lab4_P.HILInitialize_AOLow;
      lab4_DWork.HILInitialize_AOMinimums[2] = lab4_P.HILInitialize_AOLow;
      lab4_DWork.HILInitialize_AOMinimums[3] = lab4_P.HILInitialize_AOLow;
      lab4_DWork.HILInitialize_AOMaximums[0] = lab4_P.HILInitialize_AOHigh;
      lab4_DWork.HILInitialize_AOMaximums[1] = lab4_P.HILInitialize_AOHigh;
      lab4_DWork.HILInitialize_AOMaximums[2] = lab4_P.HILInitialize_AOHigh;
      lab4_DWork.HILInitialize_AOMaximums[3] = lab4_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges(lab4_DWork.HILInitialize_Card,
        lab4_P.HILInitialize_AOChannels, 4U,
        &lab4_DWork.HILInitialize_AOMinimums[0],
        &lab4_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab4_M, _rt_error_message);
        return;
      }
    }

    if ((lab4_P.HILInitialize_AOStart && !is_switching) ||
        (lab4_P.HILInitialize_AOEnter && is_switching)) {
      lab4_DWork.HILInitialize_AOVoltages[0] = lab4_P.HILInitialize_AOInitial;
      lab4_DWork.HILInitialize_AOVoltages[1] = lab4_P.HILInitialize_AOInitial;
      lab4_DWork.HILInitialize_AOVoltages[2] = lab4_P.HILInitialize_AOInitial;
      lab4_DWork.HILInitialize_AOVoltages[3] = lab4_P.HILInitialize_AOInitial;
      result = hil_write_analog(lab4_DWork.HILInitialize_Card,
        lab4_P.HILInitialize_AOChannels, 4U,
        &lab4_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab4_M, _rt_error_message);
        return;
      }
    }

    if (lab4_P.HILInitialize_AOReset) {
      lab4_DWork.HILInitialize_AOVoltages[0] = lab4_P.HILInitialize_AOWatchdog;
      lab4_DWork.HILInitialize_AOVoltages[1] = lab4_P.HILInitialize_AOWatchdog;
      lab4_DWork.HILInitialize_AOVoltages[2] = lab4_P.HILInitialize_AOWatchdog;
      lab4_DWork.HILInitialize_AOVoltages[3] = lab4_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (lab4_DWork.HILInitialize_Card, lab4_P.HILInitialize_AOChannels, 4U,
         &lab4_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab4_M, _rt_error_message);
        return;
      }
    }

    if ((lab4_P.HILInitialize_EIPStart && !is_switching) ||
        (lab4_P.HILInitialize_EIPEnter && is_switching)) {
      lab4_DWork.HILInitialize_QuadratureModes[0] =
        lab4_P.HILInitialize_EIQuadrature;
      lab4_DWork.HILInitialize_QuadratureModes[1] =
        lab4_P.HILInitialize_EIQuadrature;
      lab4_DWork.HILInitialize_QuadratureModes[2] =
        lab4_P.HILInitialize_EIQuadrature;
      lab4_DWork.HILInitialize_QuadratureModes[3] =
        lab4_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode(lab4_DWork.HILInitialize_Card,
        lab4_P.HILInitialize_EIChannels, 4U, (t_encoder_quadrature_mode *)
        &lab4_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab4_M, _rt_error_message);
        return;
      }

      lab4_DWork.HILInitialize_FilterFrequency[0] =
        lab4_P.HILInitialize_EIFrequency;
      lab4_DWork.HILInitialize_FilterFrequency[1] =
        lab4_P.HILInitialize_EIFrequency;
      lab4_DWork.HILInitialize_FilterFrequency[2] =
        lab4_P.HILInitialize_EIFrequency;
      lab4_DWork.HILInitialize_FilterFrequency[3] =
        lab4_P.HILInitialize_EIFrequency;
      result = hil_set_encoder_filter_frequency(lab4_DWork.HILInitialize_Card,
        lab4_P.HILInitialize_EIChannels, 4U,
        &lab4_DWork.HILInitialize_FilterFrequency[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab4_M, _rt_error_message);
        return;
      }
    }

    if ((lab4_P.HILInitialize_EIStart && !is_switching) ||
        (lab4_P.HILInitialize_EIEnter && is_switching)) {
      lab4_DWork.HILInitialize_InitialEICounts[0] =
        lab4_P.HILInitialize_EIInitial;
      lab4_DWork.HILInitialize_InitialEICounts[1] =
        lab4_P.HILInitialize_EIInitial;
      lab4_DWork.HILInitialize_InitialEICounts[2] =
        lab4_P.HILInitialize_EIInitial;
      lab4_DWork.HILInitialize_InitialEICounts[3] =
        lab4_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(lab4_DWork.HILInitialize_Card,
        lab4_P.HILInitialize_EIChannels, 4U,
        &lab4_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab4_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_encoder_timebase_block): '<Root>/HIL Read Encoder Timebase' */

  /* S-Function Block: lab4/HIL Read Encoder Timebase (hil_read_encoder_timebase_block) */
  {
    t_error result;
    result = hil_task_create_encoder_reader(lab4_DWork.HILInitialize_Card,
      lab4_P.HILReadEncoderTimebase_SamplesI,
      lab4_P.HILReadEncoderTimebase_Channels, 2,
      &lab4_DWork.HILReadEncoderTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab4_M, _rt_error_message);
    }
  }

  MdlInitialize();
}

void MdlTerminate(void)
{
  lab4_terminate();
}

RT_MODEL_lab4 *lab4(void)
{
  lab4_initialize(1);
  return lab4_M;
}

/*========================================================================*
 * End of GRT compatible call interface                                   *
 *========================================================================*/
