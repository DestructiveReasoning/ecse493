 mp = 0.106;
 lp = 2*0.168;
 mc = 2*0.526;
 g = 9.81;
 Km = 0.0077;
 Kg = 3.7;
 Ra = 2.6;
 r = 0.0064;
 I = (1/3)*mp*(lp)^2;
    
 range_pos = 1;          %track length of about 1m
 range_xvel = 2;         %limit to 2m/s
 range_ang = pi/6;  %+/- 10 degrees
 range_angvel = pi/2;    %limit to 60deg/s
 range_volts = 10;       %voltage in [-5,5]V
    
 A = [   0   1   0   0;
         0   -(Km^2*Kg^2/(Ra*r^2*mc))   -(mp*g/mc)  0;
         0   0   0   1;
         0   Km^2*Kg^2/(Ra*r^2*mc*lp)   (mc+mp)*g/(mc*lp)    0];
 B = [0; Kg*Km/(Ra*r*mc); 0; -Kg*Km/(Ra*r*mc*lp)];
 C = [1 0 0 0; 0 0 1 0];