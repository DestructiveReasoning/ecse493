function y = get_lead_controller(plant, wc)
    cur_phase_at_wc = 180 + 180*phase(freqresp(plant, wc)) / pi;
    phi_lead = 60 - cur_phase_at_wc;
    slead = sin(phi_lead * pi / 180);
    a = (1+slead)^2/(1-slead^2);
    ta = 1/(wc * sqrt(a));
    ungained = tf([(a*ta) 1],[ta 1]);
    cur_gain_at_wc = log(norm(freqresp(plant*ungained, wc)))/log(10);
    k = 10^(-cur_gain_at_wc);
    y = k*ungained;
end
