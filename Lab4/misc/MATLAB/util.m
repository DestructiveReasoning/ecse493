function y = closed_loop(sys)
	y = sys/(1+sys);
endfunction