function margins=get_margins()
    n=1000;
    margins = zeros(n,1);
    w = logspace(-2,2,n);
    for c = 1:n
        margins(c) = get_lc(w(c));
    end
end