/*
 * untitled2.c
 *
 * Code generation for model "untitled2.mdl".
 *
 * Model version              : 1.1
 * Simulink Coder version : 8.1 (R2011b) 08-Jul-2011
 * C source code generated on : Mon Sep 10 16:17:52 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "untitled2.h"
#include "untitled2_private.h"
#include "untitled2_dt.h"

/* Block signals (auto storage) */
BlockIO_untitled2 untitled2_B;

/* Block states (auto storage) */
D_Work_untitled2 untitled2_DWork;

/* Real-time model */
RT_MODEL_untitled2 untitled2_M_;
RT_MODEL_untitled2 *const untitled2_M = &untitled2_M_;

/* Model output function */
void untitled2_output(int_T tid)
{
  /* SignalGenerator: '<Root>/Signal Generator' */
  untitled2_B.SignalGenerator = sin(6.2831853071795862 * untitled2_M->Timing.t[0]
    * untitled2_P.SignalGenerator_Frequency) *
    untitled2_P.SignalGenerator_Amplitude;

  /* S-Function (hil_write_analog_block): '<Root>/HIL Write Analog' */

  /* S-Function Block: untitled2/HIL Write Analog (hil_write_analog_block) */
  {
    t_error result;
    result = hil_write_analog(untitled2_DWork.HILInitialize_Card,
      &untitled2_P.HILWriteAnalog_Channels, 1, &untitled2_B.SignalGenerator);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(untitled2_M, _rt_error_message);
    }
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model update function */
void untitled2_update(int_T tid)
{
  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++untitled2_M->Timing.clockTick0)) {
    ++untitled2_M->Timing.clockTickH0;
  }

  untitled2_M->Timing.t[0] = untitled2_M->Timing.clockTick0 *
    untitled2_M->Timing.stepSize0 + untitled2_M->Timing.clockTickH0 *
    untitled2_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.002s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++untitled2_M->Timing.clockTick1)) {
      ++untitled2_M->Timing.clockTickH1;
    }

    untitled2_M->Timing.t[1] = untitled2_M->Timing.clockTick1 *
      untitled2_M->Timing.stepSize1 + untitled2_M->Timing.clockTickH1 *
      untitled2_M->Timing.stepSize1 * 4294967296.0;
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model initialize function */
void untitled2_initialize(boolean_T firstTime)
{
  (void)firstTime;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)untitled2_M, 0,
                sizeof(RT_MODEL_untitled2));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&untitled2_M->solverInfo,
                          &untitled2_M->Timing.simTimeStep);
    rtsiSetTPtr(&untitled2_M->solverInfo, &rtmGetTPtr(untitled2_M));
    rtsiSetStepSizePtr(&untitled2_M->solverInfo, &untitled2_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&untitled2_M->solverInfo, (&rtmGetErrorStatus
      (untitled2_M)));
    rtsiSetRTModelPtr(&untitled2_M->solverInfo, untitled2_M);
  }

  rtsiSetSimTimeStep(&untitled2_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&untitled2_M->solverInfo,"FixedStepDiscrete");

  /* Initialize timing info */
  {
    int_T *mdlTsMap = untitled2_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    untitled2_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    untitled2_M->Timing.sampleTimes = (&untitled2_M->Timing.sampleTimesArray[0]);
    untitled2_M->Timing.offsetTimes = (&untitled2_M->Timing.offsetTimesArray[0]);

    /* task periods */
    untitled2_M->Timing.sampleTimes[0] = (0.0);
    untitled2_M->Timing.sampleTimes[1] = (0.002);

    /* task offsets */
    untitled2_M->Timing.offsetTimes[0] = (0.0);
    untitled2_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(untitled2_M, &untitled2_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = untitled2_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    untitled2_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(untitled2_M, -1);
  untitled2_M->Timing.stepSize0 = 0.002;
  untitled2_M->Timing.stepSize1 = 0.002;

  /* external mode info */
  untitled2_M->Sizes.checksums[0] = (1545777189U);
  untitled2_M->Sizes.checksums[1] = (3863603390U);
  untitled2_M->Sizes.checksums[2] = (750680040U);
  untitled2_M->Sizes.checksums[3] = (641416959U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    untitled2_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(untitled2_M->extModeInfo,
      &untitled2_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(untitled2_M->extModeInfo, untitled2_M->Sizes.checksums);
    rteiSetTPtr(untitled2_M->extModeInfo, rtmGetTPtr(untitled2_M));
  }

  untitled2_M->solverInfoPtr = (&untitled2_M->solverInfo);
  untitled2_M->Timing.stepSize = (0.002);
  rtsiSetFixedStepSize(&untitled2_M->solverInfo, 0.002);
  rtsiSetSolverMode(&untitled2_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  untitled2_M->ModelData.blockIO = ((void *) &untitled2_B);
  (void) memset(((void *) &untitled2_B), 0,
                sizeof(BlockIO_untitled2));

  /* parameters */
  untitled2_M->ModelData.defaultParam = ((real_T *)&untitled2_P);

  /* states (dwork) */
  untitled2_M->Work.dwork = ((void *) &untitled2_DWork);
  (void) memset((void *)&untitled2_DWork, 0,
                sizeof(D_Work_untitled2));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    untitled2_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 15;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }
}

/* Model terminate function */
void untitled2_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: untitled2/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(untitled2_DWork.HILInitialize_Card);
    hil_monitor_stop_all(untitled2_DWork.HILInitialize_Card);
    is_switching = false;
    if ((untitled2_P.HILInitialize_AOTerminate && !is_switching) ||
        (untitled2_P.HILInitialize_AOExit && is_switching)) {
      untitled2_DWork.HILInitialize_AOVoltages[0] =
        untitled2_P.HILInitialize_AOFinal;
      untitled2_DWork.HILInitialize_AOVoltages[1] =
        untitled2_P.HILInitialize_AOFinal;
      untitled2_DWork.HILInitialize_AOVoltages[2] =
        untitled2_P.HILInitialize_AOFinal;
      untitled2_DWork.HILInitialize_AOVoltages[3] =
        untitled2_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 4U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(untitled2_DWork.HILInitialize_Card,
        untitled2_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &untitled2_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled2_M, _rt_error_message);
      }
    }

    hil_task_delete_all(untitled2_DWork.HILInitialize_Card);
    hil_monitor_delete_all(untitled2_DWork.HILInitialize_Card);
    hil_close(untitled2_DWork.HILInitialize_Card);
    untitled2_DWork.HILInitialize_Card = NULL;
  }
}

/*========================================================================*
 * Start of GRT compatible call interface                                 *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  untitled2_output(tid);
}

void MdlUpdate(int_T tid)
{
  untitled2_update(tid);
}

void MdlInitializeSizes(void)
{
  untitled2_M->Sizes.numContStates = (0);/* Number of continuous states */
  untitled2_M->Sizes.numY = (0);       /* Number of model outputs */
  untitled2_M->Sizes.numU = (0);       /* Number of model inputs */
  untitled2_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  untitled2_M->Sizes.numSampTimes = (2);/* Number of sample times */
  untitled2_M->Sizes.numBlocks = (3);  /* Number of blocks */
  untitled2_M->Sizes.numBlockIO = (1); /* Number of block outputs */
  untitled2_M->Sizes.numBlockPrms = (74);/* Sum of parameter "widths" */
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
}

void MdlStart(void)
{
  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: untitled2/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q4", "0", &untitled2_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(untitled2_M, _rt_error_message);
      return;
    }

    is_switching = false;
    if ((untitled2_P.HILInitialize_CKPStart && !is_switching) ||
        (untitled2_P.HILInitialize_CKPEnter && is_switching)) {
      result = hil_set_clock_mode(untitled2_DWork.HILInitialize_Card, (t_clock *)
        untitled2_P.HILInitialize_CKChannels, 2U, (t_clock_mode *)
        untitled2_P.HILInitialize_CKModes);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled2_M, _rt_error_message);
        return;
      }
    }

    result = hil_watchdog_clear(untitled2_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(untitled2_M, _rt_error_message);
      return;
    }

    if ((untitled2_P.HILInitialize_AIPStart && !is_switching) ||
        (untitled2_P.HILInitialize_AIPEnter && is_switching)) {
      untitled2_DWork.HILInitialize_AIMinimums[0] =
        untitled2_P.HILInitialize_AILow;
      untitled2_DWork.HILInitialize_AIMinimums[1] =
        untitled2_P.HILInitialize_AILow;
      untitled2_DWork.HILInitialize_AIMinimums[2] =
        untitled2_P.HILInitialize_AILow;
      untitled2_DWork.HILInitialize_AIMinimums[3] =
        untitled2_P.HILInitialize_AILow;
      untitled2_DWork.HILInitialize_AIMaximums[0] =
        untitled2_P.HILInitialize_AIHigh;
      untitled2_DWork.HILInitialize_AIMaximums[1] =
        untitled2_P.HILInitialize_AIHigh;
      untitled2_DWork.HILInitialize_AIMaximums[2] =
        untitled2_P.HILInitialize_AIHigh;
      untitled2_DWork.HILInitialize_AIMaximums[3] =
        untitled2_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(untitled2_DWork.HILInitialize_Card,
        untitled2_P.HILInitialize_AIChannels, 4U,
        &untitled2_DWork.HILInitialize_AIMinimums[0],
        &untitled2_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled2_M, _rt_error_message);
        return;
      }
    }

    if ((untitled2_P.HILInitialize_AOPStart && !is_switching) ||
        (untitled2_P.HILInitialize_AOPEnter && is_switching)) {
      untitled2_DWork.HILInitialize_AOMinimums[0] =
        untitled2_P.HILInitialize_AOLow;
      untitled2_DWork.HILInitialize_AOMinimums[1] =
        untitled2_P.HILInitialize_AOLow;
      untitled2_DWork.HILInitialize_AOMinimums[2] =
        untitled2_P.HILInitialize_AOLow;
      untitled2_DWork.HILInitialize_AOMinimums[3] =
        untitled2_P.HILInitialize_AOLow;
      untitled2_DWork.HILInitialize_AOMaximums[0] =
        untitled2_P.HILInitialize_AOHigh;
      untitled2_DWork.HILInitialize_AOMaximums[1] =
        untitled2_P.HILInitialize_AOHigh;
      untitled2_DWork.HILInitialize_AOMaximums[2] =
        untitled2_P.HILInitialize_AOHigh;
      untitled2_DWork.HILInitialize_AOMaximums[3] =
        untitled2_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges(untitled2_DWork.HILInitialize_Card,
        untitled2_P.HILInitialize_AOChannels, 4U,
        &untitled2_DWork.HILInitialize_AOMinimums[0],
        &untitled2_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled2_M, _rt_error_message);
        return;
      }
    }

    if ((untitled2_P.HILInitialize_AOStart && !is_switching) ||
        (untitled2_P.HILInitialize_AOEnter && is_switching)) {
      untitled2_DWork.HILInitialize_AOVoltages[0] =
        untitled2_P.HILInitialize_AOInitial;
      untitled2_DWork.HILInitialize_AOVoltages[1] =
        untitled2_P.HILInitialize_AOInitial;
      untitled2_DWork.HILInitialize_AOVoltages[2] =
        untitled2_P.HILInitialize_AOInitial;
      untitled2_DWork.HILInitialize_AOVoltages[3] =
        untitled2_P.HILInitialize_AOInitial;
      result = hil_write_analog(untitled2_DWork.HILInitialize_Card,
        untitled2_P.HILInitialize_AOChannels, 4U,
        &untitled2_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled2_M, _rt_error_message);
        return;
      }
    }

    if (untitled2_P.HILInitialize_AOReset) {
      untitled2_DWork.HILInitialize_AOVoltages[0] =
        untitled2_P.HILInitialize_AOWatchdog;
      untitled2_DWork.HILInitialize_AOVoltages[1] =
        untitled2_P.HILInitialize_AOWatchdog;
      untitled2_DWork.HILInitialize_AOVoltages[2] =
        untitled2_P.HILInitialize_AOWatchdog;
      untitled2_DWork.HILInitialize_AOVoltages[3] =
        untitled2_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (untitled2_DWork.HILInitialize_Card,
         untitled2_P.HILInitialize_AOChannels, 4U,
         &untitled2_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled2_M, _rt_error_message);
        return;
      }
    }

    if ((untitled2_P.HILInitialize_EIPStart && !is_switching) ||
        (untitled2_P.HILInitialize_EIPEnter && is_switching)) {
      untitled2_DWork.HILInitialize_QuadratureModes[0] =
        untitled2_P.HILInitialize_EIQuadrature;
      untitled2_DWork.HILInitialize_QuadratureModes[1] =
        untitled2_P.HILInitialize_EIQuadrature;
      untitled2_DWork.HILInitialize_QuadratureModes[2] =
        untitled2_P.HILInitialize_EIQuadrature;
      untitled2_DWork.HILInitialize_QuadratureModes[3] =
        untitled2_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode
        (untitled2_DWork.HILInitialize_Card,
         untitled2_P.HILInitialize_EIChannels, 4U, (t_encoder_quadrature_mode *)
         &untitled2_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled2_M, _rt_error_message);
        return;
      }

      untitled2_DWork.HILInitialize_FilterFrequency[0] =
        untitled2_P.HILInitialize_EIFrequency;
      untitled2_DWork.HILInitialize_FilterFrequency[1] =
        untitled2_P.HILInitialize_EIFrequency;
      untitled2_DWork.HILInitialize_FilterFrequency[2] =
        untitled2_P.HILInitialize_EIFrequency;
      untitled2_DWork.HILInitialize_FilterFrequency[3] =
        untitled2_P.HILInitialize_EIFrequency;
      result = hil_set_encoder_filter_frequency
        (untitled2_DWork.HILInitialize_Card,
         untitled2_P.HILInitialize_EIChannels, 4U,
         &untitled2_DWork.HILInitialize_FilterFrequency[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled2_M, _rt_error_message);
        return;
      }
    }

    if ((untitled2_P.HILInitialize_EIStart && !is_switching) ||
        (untitled2_P.HILInitialize_EIEnter && is_switching)) {
      untitled2_DWork.HILInitialize_InitialEICounts[0] =
        untitled2_P.HILInitialize_EIInitial;
      untitled2_DWork.HILInitialize_InitialEICounts[1] =
        untitled2_P.HILInitialize_EIInitial;
      untitled2_DWork.HILInitialize_InitialEICounts[2] =
        untitled2_P.HILInitialize_EIInitial;
      untitled2_DWork.HILInitialize_InitialEICounts[3] =
        untitled2_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(untitled2_DWork.HILInitialize_Card,
        untitled2_P.HILInitialize_EIChannels, 4U,
        &untitled2_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled2_M, _rt_error_message);
        return;
      }
    }
  }

  MdlInitialize();
}

void MdlTerminate(void)
{
  untitled2_terminate();
}

RT_MODEL_untitled2 *untitled2(void)
{
  untitled2_initialize(1);
  return untitled2_M;
}

/*========================================================================*
 * End of GRT compatible call interface                                   *
 *========================================================================*/
