/*
 * untitled2_types.h
 *
 * Code generation for model "untitled2.mdl".
 *
 * Model version              : 1.1
 * Simulink Coder version : 8.1 (R2011b) 08-Jul-2011
 * C source code generated on : Mon Sep 10 16:17:52 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_untitled2_types_h_
#define RTW_HEADER_untitled2_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_untitled2_ Parameters_untitled2;

/* Forward declaration for rtModel */
typedef struct RT_MODEL_untitled2 RT_MODEL_untitled2;

#endif                                 /* RTW_HEADER_untitled2_types_h_ */
