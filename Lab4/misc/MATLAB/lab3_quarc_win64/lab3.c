/*
 * lab3.c
 *
 * Code generation for model "lab3.mdl".
 *
 * Model version              : 1.157
 * Simulink Coder version : 8.1 (R2011b) 08-Jul-2011
 * C source code generated on : Wed Nov 07 13:03:58 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "lab3.h"
#include "lab3_private.h"
#include "lab3_dt.h"

/* Block signals (auto storage) */
BlockIO_lab3 lab3_B;

/* Continuous states */
ContinuousStates_lab3 lab3_X;

/* Block states (auto storage) */
D_Work_lab3 lab3_DWork;

/* Real-time model */
RT_MODEL_lab3 lab3_M_;
RT_MODEL_lab3 *const lab3_M = &lab3_M_;

/*
 * This function updates continuous states using the ODE1 fixed-step
 * solver algorithm
 */
static void rt_ertODEUpdateContinuousStates(RTWSolverInfo *si )
{
  time_T tnew = rtsiGetSolverStopTime(si);
  time_T h = rtsiGetStepSize(si);
  real_T *x = rtsiGetContStates(si);
  ODE1_IntgData *id = (ODE1_IntgData *)rtsiGetSolverData(si);
  real_T *f0 = id->f[0];
  int_T i;
  int_T nXc = 1;
  rtsiSetSimTimeStep(si,MINOR_TIME_STEP);
  rtsiSetdX(si, f0);
  lab3_derivatives();
  rtsiSetT(si, tnew);
  for (i = 0; i < nXc; i++) {
    *x += h * f0[i];
    x++;
  }

  rtsiSetSimTimeStep(si,MAJOR_TIME_STEP);
}

/* Model output function */
void lab3_output(int_T tid)
{
  /* local block i/o variables */
  real_T rtb_TransferFcn;
  real_T rtb_Sum4;
  real_T rtb_K1;
  int32_T cff;
  int32_T j;
  if (rtmIsMajorTimeStep(lab3_M)) {
    /* set solver stop time */
    if (!(lab3_M->Timing.clockTick0+1)) {
      rtsiSetSolverStopTime(&lab3_M->solverInfo, ((lab3_M->Timing.clockTickH0 +
        1) * lab3_M->Timing.stepSize0 * 4294967296.0));
    } else {
      rtsiSetSolverStopTime(&lab3_M->solverInfo, ((lab3_M->Timing.clockTick0 + 1)
        * lab3_M->Timing.stepSize0 + lab3_M->Timing.clockTickH0 *
        lab3_M->Timing.stepSize0 * 4294967296.0));
    }
  }                                    /* end MajorTimeStep */

  /* Update absolute time of base rate at minor time step */
  if (rtmIsMinorTimeStep(lab3_M)) {
    lab3_M->Timing.t[0] = rtsiGetT(&lab3_M->solverInfo);
  }

  if (rtmIsMajorTimeStep(lab3_M)) {
    /* S-Function (hil_read_encoder_timebase_block): '<Root>/HIL Read Encoder Timebase' */

    /* S-Function Block: lab3/HIL Read Encoder Timebase (hil_read_encoder_timebase_block) */
    {
      t_error result;
      result = hil_task_read_encoder(lab3_DWork.HILReadEncoderTimebase_Task, 1,
        &lab3_DWork.HILReadEncoderTimebase_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
      } else {
        lab3_B.HILReadEncoderTimebase = lab3_DWork.HILReadEncoderTimebase_Buffer;
      }
    }

    /* Sum: '<Root>/Sum3' incorporates:
     *  Constant: '<Root>/Constant'
     */
    lab3_B.Sum3 = lab3_P.Constant_Value + 0.0;

    /* Gain: '<Root>/Gain1' */
    lab3_B.Gain1 = lab3_P.Gain1_Gain * lab3_B.HILReadEncoderTimebase;

    /* Sum: '<Root>/Sum' */
    lab3_B.Sum = lab3_B.Sum3 - lab3_B.Gain1;

    /* SampleTimeMath: '<S2>/TSamp'
     *
     * About '<S2>/TSamp':
     *  y = u * K where K = 1 / ( w * Ts )
     */
    lab3_B.TSamp = lab3_B.Sum * lab3_P.TSamp_WtEt;

    /* DiscreteIntegrator: '<Root>/Discrete-Time Integrator' */
    rtb_Sum4 = lab3_DWork.DiscreteTimeIntegrator_DSTATE;

    /* Gain: '<Root>/K1' incorporates:
     *  Gain: '<Root>/K'
     *  Gain: '<Root>/Kd'
     *  Gain: '<Root>/Ki'
     *  Sum: '<Root>/Sum1'
     *  Sum: '<Root>/Sum2'
     *  Sum: '<S2>/Diff'
     *  UnitDelay: '<S2>/UD'
     */
    rtb_K1 = (((lab3_B.TSamp - lab3_DWork.UD_DSTATE) * lab3_P.Kd_Gain +
               lab3_P.Ki_Gain * rtb_Sum4) + lab3_P.K_Gain * lab3_B.Sum) *
      lab3_P.K1_Gain;
  }

  /* TransferFcn: '<Root>/Transfer Fcn' */
  rtb_TransferFcn = lab3_P.TransferFcn_D*lab3_B.Sum;
  rtb_TransferFcn += lab3_P.TransferFcn_C*lab3_X.TransferFcn_CSTATE;

  /* Gain: '<Root>/K2' */
  lab3_B.K2 = lab3_P.K2_Gain * rtb_TransferFcn;
  if (rtmIsMajorTimeStep(lab3_M)) {
    /* Sum: '<Root>/Sum4' */
    rtb_Sum4 = rtb_K1 + lab3_B.K2;

    /* S-Function (hil_write_analog_block): '<Root>/HIL Write Analog' */

    /* S-Function Block: lab3/HIL Write Analog (hil_write_analog_block) */
    {
      t_error result;
      result = hil_write_analog(lab3_DWork.HILInitialize_Card,
        &lab3_P.HILWriteAnalog_Channels, 1, &rtb_Sum4);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
      }
    }

    /* S-Function (hil_read_encoder_block): '<Root>/HIL Read Encoder' */

    /* S-Function Block: lab3/HIL Read Encoder (hil_read_encoder_block) */
    {
      t_error result = hil_read_encoder(lab3_DWork.HILInitialize_Card,
        &lab3_P.HILReadEncoder_Channels, 1, &lab3_DWork.HILReadEncoder_Buffer);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
      } else {
        lab3_B.HILReadEncoder = lab3_DWork.HILReadEncoder_Buffer;
      }
    }

    /* SampleTimeMath: '<S1>/TSamp'
     *
     * About '<S1>/TSamp':
     *  y = u * K where K = 1 / ( w * Ts )
     */
    lab3_B.TSamp_m = lab3_B.HILReadEncoder * lab3_P.TSamp_WtEt_p;

    /* Sum: '<S1>/Diff' incorporates:
     *  UnitDelay: '<S1>/UD'
     */
    lab3_B.Diff = lab3_B.TSamp_m - lab3_DWork.UD_DSTATE_a;

    /* DiscreteFir: '<Root>/Discrete FIR Filter' */
    rtb_K1 = lab3_B.Diff * lab3_P.DiscreteFIRFilter_Coefficients[0];
    cff = 1;
    for (j = lab3_DWork.DiscreteFIRFilter_circBuf; j < 4; j++) {
      rtb_K1 += lab3_DWork.DiscreteFIRFilter_states[j] *
        lab3_P.DiscreteFIRFilter_Coefficients[cff];
      cff++;
    }

    for (j = 0; j < lab3_DWork.DiscreteFIRFilter_circBuf; j++) {
      rtb_K1 += lab3_DWork.DiscreteFIRFilter_states[j] *
        lab3_P.DiscreteFIRFilter_Coefficients[cff];
      cff++;
    }

    lab3_B.DiscreteFIRFilter = rtb_K1;

    /* End of DiscreteFir: '<Root>/Discrete FIR Filter' */
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model update function */
void lab3_update(int_T tid)
{
  if (rtmIsMajorTimeStep(lab3_M)) {
    /* Update for UnitDelay: '<S2>/UD' */
    lab3_DWork.UD_DSTATE = lab3_B.TSamp;

    /* Update for DiscreteIntegrator: '<Root>/Discrete-Time Integrator' */
    lab3_DWork.DiscreteTimeIntegrator_DSTATE =
      lab3_P.DiscreteTimeIntegrator_gainval * lab3_B.Sum +
      lab3_DWork.DiscreteTimeIntegrator_DSTATE;

    /* Update for UnitDelay: '<S1>/UD' */
    lab3_DWork.UD_DSTATE_a = lab3_B.TSamp_m;

    /* Update for DiscreteFir: '<Root>/Discrete FIR Filter' */
    lab3_DWork.DiscreteFIRFilter_circBuf = lab3_DWork.DiscreteFIRFilter_circBuf
      - 1;
    if (lab3_DWork.DiscreteFIRFilter_circBuf < 0) {
      lab3_DWork.DiscreteFIRFilter_circBuf = 3;
    }

    lab3_DWork.DiscreteFIRFilter_states[lab3_DWork.DiscreteFIRFilter_circBuf] =
      lab3_B.Diff;

    /* End of Update for DiscreteFir: '<Root>/Discrete FIR Filter' */
  }

  if (rtmIsMajorTimeStep(lab3_M)) {
    rt_ertODEUpdateContinuousStates(&lab3_M->solverInfo);
  }

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++lab3_M->Timing.clockTick0)) {
    ++lab3_M->Timing.clockTickH0;
  }

  lab3_M->Timing.t[0] = rtsiGetSolverStopTime(&lab3_M->solverInfo);

  {
    /* Update absolute timer for sample time: [0.002s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++lab3_M->Timing.clockTick1)) {
      ++lab3_M->Timing.clockTickH1;
    }

    lab3_M->Timing.t[1] = lab3_M->Timing.clockTick1 * lab3_M->Timing.stepSize1 +
      lab3_M->Timing.clockTickH1 * lab3_M->Timing.stepSize1 * 4294967296.0;
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Derivatives for root system: '<Root>' */
void lab3_derivatives(void)
{
  /* Derivatives for TransferFcn: '<Root>/Transfer Fcn' */
  {
    ((StateDerivatives_lab3 *) lab3_M->ModelData.derivs)->TransferFcn_CSTATE =
      lab3_B.Sum;
    ((StateDerivatives_lab3 *) lab3_M->ModelData.derivs)->TransferFcn_CSTATE +=
      (lab3_P.TransferFcn_A)*lab3_X.TransferFcn_CSTATE;
  }
}

/* Model initialize function */
void lab3_initialize(boolean_T firstTime)
{
  (void)firstTime;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)lab3_M, 0,
                sizeof(RT_MODEL_lab3));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&lab3_M->solverInfo, &lab3_M->Timing.simTimeStep);
    rtsiSetTPtr(&lab3_M->solverInfo, &rtmGetTPtr(lab3_M));
    rtsiSetStepSizePtr(&lab3_M->solverInfo, &lab3_M->Timing.stepSize0);
    rtsiSetdXPtr(&lab3_M->solverInfo, &lab3_M->ModelData.derivs);
    rtsiSetContStatesPtr(&lab3_M->solverInfo, &lab3_M->ModelData.contStates);
    rtsiSetNumContStatesPtr(&lab3_M->solverInfo, &lab3_M->Sizes.numContStates);
    rtsiSetErrorStatusPtr(&lab3_M->solverInfo, (&rtmGetErrorStatus(lab3_M)));
    rtsiSetRTModelPtr(&lab3_M->solverInfo, lab3_M);
  }

  rtsiSetSimTimeStep(&lab3_M->solverInfo, MAJOR_TIME_STEP);
  lab3_M->ModelData.intgData.f[0] = lab3_M->ModelData.odeF[0];
  lab3_M->ModelData.contStates = ((real_T *) &lab3_X);
  rtsiSetSolverData(&lab3_M->solverInfo, (void *)&lab3_M->ModelData.intgData);
  rtsiSetSolverName(&lab3_M->solverInfo,"ode1");

  /* Initialize timing info */
  {
    int_T *mdlTsMap = lab3_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    lab3_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    lab3_M->Timing.sampleTimes = (&lab3_M->Timing.sampleTimesArray[0]);
    lab3_M->Timing.offsetTimes = (&lab3_M->Timing.offsetTimesArray[0]);

    /* task periods */
    lab3_M->Timing.sampleTimes[0] = (0.0);
    lab3_M->Timing.sampleTimes[1] = (0.002);

    /* task offsets */
    lab3_M->Timing.offsetTimes[0] = (0.0);
    lab3_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(lab3_M, &lab3_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = lab3_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    lab3_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(lab3_M, -1);
  lab3_M->Timing.stepSize0 = 0.002;
  lab3_M->Timing.stepSize1 = 0.002;

  /* external mode info */
  lab3_M->Sizes.checksums[0] = (2367599588U);
  lab3_M->Sizes.checksums[1] = (1402768800U);
  lab3_M->Sizes.checksums[2] = (529832475U);
  lab3_M->Sizes.checksums[3] = (145578713U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    lab3_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(lab3_M->extModeInfo,
      &lab3_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(lab3_M->extModeInfo, lab3_M->Sizes.checksums);
    rteiSetTPtr(lab3_M->extModeInfo, rtmGetTPtr(lab3_M));
  }

  lab3_M->solverInfoPtr = (&lab3_M->solverInfo);
  lab3_M->Timing.stepSize = (0.002);
  rtsiSetFixedStepSize(&lab3_M->solverInfo, 0.002);
  rtsiSetSolverMode(&lab3_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  lab3_M->ModelData.blockIO = ((void *) &lab3_B);
  (void) memset(((void *) &lab3_B), 0,
                sizeof(BlockIO_lab3));

  /* parameters */
  lab3_M->ModelData.defaultParam = ((real_T *)&lab3_P);

  /* states (continuous) */
  {
    real_T *x = (real_T *) &lab3_X;
    lab3_M->ModelData.contStates = (x);
    (void) memset((void *)&lab3_X, 0,
                  sizeof(ContinuousStates_lab3));
  }

  /* states (dwork) */
  lab3_M->Work.dwork = ((void *) &lab3_DWork);
  (void) memset((void *)&lab3_DWork, 0,
                sizeof(D_Work_lab3));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    lab3_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }
}

/* Model terminate function */
void lab3_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: lab3/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(lab3_DWork.HILInitialize_Card);
    hil_monitor_stop_all(lab3_DWork.HILInitialize_Card);
    is_switching = false;
    if ((lab3_P.HILInitialize_AOTerminate && !is_switching) ||
        (lab3_P.HILInitialize_AOExit && is_switching)) {
      lab3_DWork.HILInitialize_AOVoltages[0] = lab3_P.HILInitialize_AOFinal;
      lab3_DWork.HILInitialize_AOVoltages[1] = lab3_P.HILInitialize_AOFinal;
      lab3_DWork.HILInitialize_AOVoltages[2] = lab3_P.HILInitialize_AOFinal;
      lab3_DWork.HILInitialize_AOVoltages[3] = lab3_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 4U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(lab3_DWork.HILInitialize_Card,
        lab3_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &lab3_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
      }
    }

    hil_task_delete_all(lab3_DWork.HILInitialize_Card);
    hil_monitor_delete_all(lab3_DWork.HILInitialize_Card);
    hil_close(lab3_DWork.HILInitialize_Card);
    lab3_DWork.HILInitialize_Card = NULL;
  }
}

/*========================================================================*
 * Start of GRT compatible call interface                                 *
 *========================================================================*/

/* Solver interface called by GRT_Main */
#ifndef USE_GENERATED_SOLVER

void rt_ODECreateIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEDestroyIntegrationData(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

void rt_ODEUpdateContinuousStates(RTWSolverInfo *si)
{
  UNUSED_PARAMETER(si);
  return;
}                                      /* do nothing */

#endif

void MdlOutputs(int_T tid)
{
  lab3_output(tid);
}

void MdlUpdate(int_T tid)
{
  lab3_update(tid);
}

void MdlInitializeSizes(void)
{
  lab3_M->Sizes.numContStates = (1);   /* Number of continuous states */
  lab3_M->Sizes.numY = (0);            /* Number of model outputs */
  lab3_M->Sizes.numU = (0);            /* Number of model inputs */
  lab3_M->Sizes.sysDirFeedThru = (0);  /* The model is not direct feedthrough */
  lab3_M->Sizes.numSampTimes = (2);    /* Number of sample times */
  lab3_M->Sizes.numBlocks = (35);      /* Number of blocks */
  lab3_M->Sizes.numBlockIO = (10);     /* Number of block outputs */
  lab3_M->Sizes.numBlockPrms = (100);  /* Sum of parameter "widths" */
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
  /* InitializeConditions for UnitDelay: '<S2>/UD' */
  lab3_DWork.UD_DSTATE = lab3_P.UD_X0;

  /* InitializeConditions for DiscreteIntegrator: '<Root>/Discrete-Time Integrator' */
  lab3_DWork.DiscreteTimeIntegrator_DSTATE = lab3_P.DiscreteTimeIntegrator_IC;

  /* InitializeConditions for TransferFcn: '<Root>/Transfer Fcn' */
  lab3_X.TransferFcn_CSTATE = 0.0;

  /* InitializeConditions for UnitDelay: '<S1>/UD' */
  lab3_DWork.UD_DSTATE_a = lab3_P.UD_X0_m;

  /* InitializeConditions for DiscreteFir: '<Root>/Discrete FIR Filter' */
  lab3_DWork.DiscreteFIRFilter_states[0] =
    lab3_P.DiscreteFIRFilter_InitialStates;
  lab3_DWork.DiscreteFIRFilter_states[1] =
    lab3_P.DiscreteFIRFilter_InitialStates;
  lab3_DWork.DiscreteFIRFilter_states[2] =
    lab3_P.DiscreteFIRFilter_InitialStates;
  lab3_DWork.DiscreteFIRFilter_states[3] =
    lab3_P.DiscreteFIRFilter_InitialStates;
  lab3_DWork.DiscreteFIRFilter_circBuf = 0;
}

void MdlStart(void)
{
  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: lab3/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q4", "0", &lab3_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab3_M, _rt_error_message);
      return;
    }

    is_switching = false;
    if ((lab3_P.HILInitialize_CKPStart && !is_switching) ||
        (lab3_P.HILInitialize_CKPEnter && is_switching)) {
      result = hil_set_clock_mode(lab3_DWork.HILInitialize_Card, (t_clock *)
        lab3_P.HILInitialize_CKChannels, 2U, (t_clock_mode *)
        lab3_P.HILInitialize_CKModes);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
        return;
      }
    }

    result = hil_watchdog_clear(lab3_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab3_M, _rt_error_message);
      return;
    }

    if ((lab3_P.HILInitialize_AIPStart && !is_switching) ||
        (lab3_P.HILInitialize_AIPEnter && is_switching)) {
      lab3_DWork.HILInitialize_AIMinimums[0] = lab3_P.HILInitialize_AILow;
      lab3_DWork.HILInitialize_AIMinimums[1] = lab3_P.HILInitialize_AILow;
      lab3_DWork.HILInitialize_AIMinimums[2] = lab3_P.HILInitialize_AILow;
      lab3_DWork.HILInitialize_AIMinimums[3] = lab3_P.HILInitialize_AILow;
      lab3_DWork.HILInitialize_AIMaximums[0] = lab3_P.HILInitialize_AIHigh;
      lab3_DWork.HILInitialize_AIMaximums[1] = lab3_P.HILInitialize_AIHigh;
      lab3_DWork.HILInitialize_AIMaximums[2] = lab3_P.HILInitialize_AIHigh;
      lab3_DWork.HILInitialize_AIMaximums[3] = lab3_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(lab3_DWork.HILInitialize_Card,
        lab3_P.HILInitialize_AIChannels, 4U,
        &lab3_DWork.HILInitialize_AIMinimums[0],
        &lab3_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
        return;
      }
    }

    if ((lab3_P.HILInitialize_AOPStart && !is_switching) ||
        (lab3_P.HILInitialize_AOPEnter && is_switching)) {
      lab3_DWork.HILInitialize_AOMinimums[0] = lab3_P.HILInitialize_AOLow;
      lab3_DWork.HILInitialize_AOMinimums[1] = lab3_P.HILInitialize_AOLow;
      lab3_DWork.HILInitialize_AOMinimums[2] = lab3_P.HILInitialize_AOLow;
      lab3_DWork.HILInitialize_AOMinimums[3] = lab3_P.HILInitialize_AOLow;
      lab3_DWork.HILInitialize_AOMaximums[0] = lab3_P.HILInitialize_AOHigh;
      lab3_DWork.HILInitialize_AOMaximums[1] = lab3_P.HILInitialize_AOHigh;
      lab3_DWork.HILInitialize_AOMaximums[2] = lab3_P.HILInitialize_AOHigh;
      lab3_DWork.HILInitialize_AOMaximums[3] = lab3_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges(lab3_DWork.HILInitialize_Card,
        lab3_P.HILInitialize_AOChannels, 4U,
        &lab3_DWork.HILInitialize_AOMinimums[0],
        &lab3_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
        return;
      }
    }

    if ((lab3_P.HILInitialize_AOStart && !is_switching) ||
        (lab3_P.HILInitialize_AOEnter && is_switching)) {
      lab3_DWork.HILInitialize_AOVoltages[0] = lab3_P.HILInitialize_AOInitial;
      lab3_DWork.HILInitialize_AOVoltages[1] = lab3_P.HILInitialize_AOInitial;
      lab3_DWork.HILInitialize_AOVoltages[2] = lab3_P.HILInitialize_AOInitial;
      lab3_DWork.HILInitialize_AOVoltages[3] = lab3_P.HILInitialize_AOInitial;
      result = hil_write_analog(lab3_DWork.HILInitialize_Card,
        lab3_P.HILInitialize_AOChannels, 4U,
        &lab3_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
        return;
      }
    }

    if (lab3_P.HILInitialize_AOReset) {
      lab3_DWork.HILInitialize_AOVoltages[0] = lab3_P.HILInitialize_AOWatchdog;
      lab3_DWork.HILInitialize_AOVoltages[1] = lab3_P.HILInitialize_AOWatchdog;
      lab3_DWork.HILInitialize_AOVoltages[2] = lab3_P.HILInitialize_AOWatchdog;
      lab3_DWork.HILInitialize_AOVoltages[3] = lab3_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (lab3_DWork.HILInitialize_Card, lab3_P.HILInitialize_AOChannels, 4U,
         &lab3_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
        return;
      }
    }

    if ((lab3_P.HILInitialize_EIPStart && !is_switching) ||
        (lab3_P.HILInitialize_EIPEnter && is_switching)) {
      lab3_DWork.HILInitialize_QuadratureModes[0] =
        lab3_P.HILInitialize_EIQuadrature;
      lab3_DWork.HILInitialize_QuadratureModes[1] =
        lab3_P.HILInitialize_EIQuadrature;
      lab3_DWork.HILInitialize_QuadratureModes[2] =
        lab3_P.HILInitialize_EIQuadrature;
      lab3_DWork.HILInitialize_QuadratureModes[3] =
        lab3_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode(lab3_DWork.HILInitialize_Card,
        lab3_P.HILInitialize_EIChannels, 4U, (t_encoder_quadrature_mode *)
        &lab3_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
        return;
      }

      lab3_DWork.HILInitialize_FilterFrequency[0] =
        lab3_P.HILInitialize_EIFrequency;
      lab3_DWork.HILInitialize_FilterFrequency[1] =
        lab3_P.HILInitialize_EIFrequency;
      lab3_DWork.HILInitialize_FilterFrequency[2] =
        lab3_P.HILInitialize_EIFrequency;
      lab3_DWork.HILInitialize_FilterFrequency[3] =
        lab3_P.HILInitialize_EIFrequency;
      result = hil_set_encoder_filter_frequency(lab3_DWork.HILInitialize_Card,
        lab3_P.HILInitialize_EIChannels, 4U,
        &lab3_DWork.HILInitialize_FilterFrequency[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
        return;
      }
    }

    if ((lab3_P.HILInitialize_EIStart && !is_switching) ||
        (lab3_P.HILInitialize_EIEnter && is_switching)) {
      lab3_DWork.HILInitialize_InitialEICounts[0] =
        lab3_P.HILInitialize_EIInitial;
      lab3_DWork.HILInitialize_InitialEICounts[1] =
        lab3_P.HILInitialize_EIInitial;
      lab3_DWork.HILInitialize_InitialEICounts[2] =
        lab3_P.HILInitialize_EIInitial;
      lab3_DWork.HILInitialize_InitialEICounts[3] =
        lab3_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(lab3_DWork.HILInitialize_Card,
        lab3_P.HILInitialize_EIChannels, 4U,
        &lab3_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab3_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_encoder_timebase_block): '<Root>/HIL Read Encoder Timebase' */

  /* S-Function Block: lab3/HIL Read Encoder Timebase (hil_read_encoder_timebase_block) */
  {
    t_error result;
    result = hil_task_create_encoder_reader(lab3_DWork.HILInitialize_Card,
      lab3_P.HILReadEncoderTimebase_SamplesI,
      &lab3_P.HILReadEncoderTimebase_Channels, 1,
      &lab3_DWork.HILReadEncoderTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab3_M, _rt_error_message);
    }
  }

  MdlInitialize();
}

void MdlTerminate(void)
{
  lab3_terminate();
}

RT_MODEL_lab3 *lab3(void)
{
  lab3_initialize(1);
  return lab3_M;
}

/*========================================================================*
 * End of GRT compatible call interface                                   *
 *========================================================================*/
