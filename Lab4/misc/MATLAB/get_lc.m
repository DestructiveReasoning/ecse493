function y = get_lc(wc)
    plant = tf(10*3.255,[1,14.15,0]);
    [gm, pm] = margin(plant);
    phi_lead = 60 - pm;
    slead = sin(phi_lead * pi / 180);
    a = (1-slead)/(1+slead);
    ta = 1/(wc * sqrt(a));
    y = tf([(a*ta) 1],[ta 1]);
    resp = plant*y;
    [ngm,npm] = margin(resp);
    y=npm;
end