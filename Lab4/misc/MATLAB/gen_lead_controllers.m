function y = gen_lead_controllers(plant, wcs)
    for c = 1:length(wcs)
        y = get_lead_controller(plant, wcs(c));
        controller = (plant*y)/(1 + plant*y);
        step(0.2 * controller, linspace(0,0.25,10000));
        if c == 1
            hold;
        end
    end
    legend(arrayfun(@(x) strcat('wc = ',num2str(x)), wcs, 'un', 0));
end