/*
 * lab2.c
 *
 * Code generation for model "lab2.mdl".
 *
 * Model version              : 1.99
 * Simulink Coder version : 8.1 (R2011b) 08-Jul-2011
 * C source code generated on : Mon Oct 22 10:17:08 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "lab2.h"
#include "lab2_private.h"
#include "lab2_dt.h"

const real_T lab2_RGND = 0.0;          /* real_T ground */

/* Block signals (auto storage) */
BlockIO_lab2 lab2_B;

/* Block states (auto storage) */
D_Work_lab2 lab2_DWork;

/* Real-time model */
RT_MODEL_lab2 lab2_M_;
RT_MODEL_lab2 *const lab2_M = &lab2_M_;

/* Model output function */
void lab2_output(int_T tid)
{
  int32_T cff;
  real_T acc;
  int32_T j;

  /* S-Function (hil_read_encoder_timebase_block): '<Root>/HIL Read Encoder Timebase' */

  /* S-Function Block: lab2/HIL Read Encoder Timebase (hil_read_encoder_timebase_block) */
  {
    t_error result;
    result = hil_task_read_encoder(lab2_DWork.HILReadEncoderTimebase_Task, 1,
      &lab2_DWork.HILReadEncoderTimebase_Buffer);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab2_M, _rt_error_message);
    } else {
      lab2_B.HILReadEncoderTimebase = lab2_DWork.HILReadEncoderTimebase_Buffer;
    }
  }

  /* S-Function (hil_write_analog_block): '<Root>/HIL Write Analog' */

  /* S-Function Block: lab2/HIL Write Analog (hil_write_analog_block) */
  {
    t_error result;
    result = hil_write_analog(lab2_DWork.HILInitialize_Card,
      &lab2_P.HILWriteAnalog_Channels, 1, (real_T*)&lab2_RGND);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab2_M, _rt_error_message);
    }
  }

  /* S-Function (hil_read_encoder_block): '<Root>/HIL Read Encoder' */

  /* S-Function Block: lab2/HIL Read Encoder (hil_read_encoder_block) */
  {
    t_error result = hil_read_encoder(lab2_DWork.HILInitialize_Card,
      &lab2_P.HILReadEncoder_Channels, 1, &lab2_DWork.HILReadEncoder_Buffer);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab2_M, _rt_error_message);
    } else {
      lab2_B.HILReadEncoder = lab2_DWork.HILReadEncoder_Buffer;
    }
  }

  /* SampleTimeMath: '<S1>/TSamp'
   *
   * About '<S1>/TSamp':
   *  y = u * K where K = 1 / ( w * Ts )
   */
  lab2_B.TSamp = lab2_B.HILReadEncoder * lab2_P.TSamp_WtEt;

  /* Sum: '<S1>/Diff' incorporates:
   *  UnitDelay: '<S1>/UD'
   */
  lab2_B.Diff = lab2_B.TSamp - lab2_DWork.UD_DSTATE;

  /* DiscreteFir: '<Root>/Discrete FIR Filter' */
  acc = lab2_B.Diff * lab2_P.DiscreteFIRFilter_Coefficients[0];
  cff = 1;
  for (j = lab2_DWork.DiscreteFIRFilter_circBuf; j < 4; j++) {
    acc += lab2_DWork.DiscreteFIRFilter_states[j] *
      lab2_P.DiscreteFIRFilter_Coefficients[cff];
    cff++;
  }

  for (j = 0; j < lab2_DWork.DiscreteFIRFilter_circBuf; j++) {
    acc += lab2_DWork.DiscreteFIRFilter_states[j] *
      lab2_P.DiscreteFIRFilter_Coefficients[cff];
    cff++;
  }

  lab2_B.DiscreteFIRFilter = acc;

  /* End of DiscreteFir: '<Root>/Discrete FIR Filter' */

  /* Gain: '<Root>/Gain1' */
  lab2_B.Gain1 = lab2_P.Gain1_Gain * lab2_B.HILReadEncoderTimebase;

  /* SignalGenerator: '<Root>/Signal Generator' */
  lab2_B.SignalGenerator = sin(6.2831853071795862 * lab2_M->Timing.t[0] *
    lab2_P.SignalGenerator_Frequency) * lab2_P.SignalGenerator_Amplitude;

  /* Sum: '<Root>/Sum' */
  lab2_B.Sum = lab2_B.SignalGenerator - lab2_B.Gain1;

  /* Gain: '<Root>/Gain' */
  lab2_B.Gain = lab2_P.Gain_Gain * lab2_B.Sum;

  /* S-Function (hil_read_analog_block): '<Root>/HIL Read Analog' */

  /* S-Function Block: lab2/HIL Read Analog (hil_read_analog_block) */
  {
    t_error result = hil_read_analog(lab2_DWork.HILInitialize_Card,
      &lab2_P.HILReadAnalog_Channels, 1, &lab2_DWork.HILReadAnalog_Buffer);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab2_M, _rt_error_message);
    }

    lab2_B.HILReadAnalog = lab2_DWork.HILReadAnalog_Buffer;
  }

  /* SignalGenerator: '<Root>/Signal Generator1' */
  acc = lab2_P.SignalGenerator1_Frequency * lab2_M->Timing.t[0];
  if (acc - floor(acc) >= 0.5) {
    lab2_B.SignalGenerator1 = lab2_P.SignalGenerator1_Amplitude;
  } else {
    lab2_B.SignalGenerator1 = -lab2_P.SignalGenerator1_Amplitude;
  }

  /* End of SignalGenerator: '<Root>/Signal Generator1' */

  /* S-Function (hil_write_analog_block): '<Root>/HIL Write Analog1' */

  /* S-Function Block: lab2/HIL Write Analog1 (hil_write_analog_block) */
  {
    t_error result;
    result = hil_write_analog(lab2_DWork.HILInitialize_Card,
      &lab2_P.HILWriteAnalog1_Channels, 1, &lab2_B.SignalGenerator1);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab2_M, _rt_error_message);
    }
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model update function */
void lab2_update(int_T tid)
{
  /* Update for UnitDelay: '<S1>/UD' */
  lab2_DWork.UD_DSTATE = lab2_B.TSamp;

  /* Update for DiscreteFir: '<Root>/Discrete FIR Filter' */
  lab2_DWork.DiscreteFIRFilter_circBuf = lab2_DWork.DiscreteFIRFilter_circBuf -
    1;
  if (lab2_DWork.DiscreteFIRFilter_circBuf < 0) {
    lab2_DWork.DiscreteFIRFilter_circBuf = 3;
  }

  lab2_DWork.DiscreteFIRFilter_states[lab2_DWork.DiscreteFIRFilter_circBuf] =
    lab2_B.Diff;

  /* End of Update for DiscreteFir: '<Root>/Discrete FIR Filter' */

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++lab2_M->Timing.clockTick0)) {
    ++lab2_M->Timing.clockTickH0;
  }

  lab2_M->Timing.t[0] = lab2_M->Timing.clockTick0 * lab2_M->Timing.stepSize0 +
    lab2_M->Timing.clockTickH0 * lab2_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.002s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++lab2_M->Timing.clockTick1)) {
      ++lab2_M->Timing.clockTickH1;
    }

    lab2_M->Timing.t[1] = lab2_M->Timing.clockTick1 * lab2_M->Timing.stepSize1 +
      lab2_M->Timing.clockTickH1 * lab2_M->Timing.stepSize1 * 4294967296.0;
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model initialize function */
void lab2_initialize(boolean_T firstTime)
{
  (void)firstTime;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)lab2_M, 0,
                sizeof(RT_MODEL_lab2));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&lab2_M->solverInfo, &lab2_M->Timing.simTimeStep);
    rtsiSetTPtr(&lab2_M->solverInfo, &rtmGetTPtr(lab2_M));
    rtsiSetStepSizePtr(&lab2_M->solverInfo, &lab2_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&lab2_M->solverInfo, (&rtmGetErrorStatus(lab2_M)));
    rtsiSetRTModelPtr(&lab2_M->solverInfo, lab2_M);
  }

  rtsiSetSimTimeStep(&lab2_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&lab2_M->solverInfo,"FixedStepDiscrete");

  /* Initialize timing info */
  {
    int_T *mdlTsMap = lab2_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    lab2_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    lab2_M->Timing.sampleTimes = (&lab2_M->Timing.sampleTimesArray[0]);
    lab2_M->Timing.offsetTimes = (&lab2_M->Timing.offsetTimesArray[0]);

    /* task periods */
    lab2_M->Timing.sampleTimes[0] = (0.0);
    lab2_M->Timing.sampleTimes[1] = (0.002);

    /* task offsets */
    lab2_M->Timing.offsetTimes[0] = (0.0);
    lab2_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(lab2_M, &lab2_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = lab2_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    lab2_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(lab2_M, -1);
  lab2_M->Timing.stepSize0 = 0.002;
  lab2_M->Timing.stepSize1 = 0.002;

  /* external mode info */
  lab2_M->Sizes.checksums[0] = (3006887257U);
  lab2_M->Sizes.checksums[1] = (4067650981U);
  lab2_M->Sizes.checksums[2] = (3133502042U);
  lab2_M->Sizes.checksums[3] = (1950457418U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    lab2_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(lab2_M->extModeInfo,
      &lab2_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(lab2_M->extModeInfo, lab2_M->Sizes.checksums);
    rteiSetTPtr(lab2_M->extModeInfo, rtmGetTPtr(lab2_M));
  }

  lab2_M->solverInfoPtr = (&lab2_M->solverInfo);
  lab2_M->Timing.stepSize = (0.002);
  rtsiSetFixedStepSize(&lab2_M->solverInfo, 0.002);
  rtsiSetSolverMode(&lab2_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  lab2_M->ModelData.blockIO = ((void *) &lab2_B);
  (void) memset(((void *) &lab2_B), 0,
                sizeof(BlockIO_lab2));

  /* parameters */
  lab2_M->ModelData.defaultParam = ((real_T *)&lab2_P);

  /* states (dwork) */
  lab2_M->Work.dwork = ((void *) &lab2_DWork);
  (void) memset((void *)&lab2_DWork, 0,
                sizeof(D_Work_lab2));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    lab2_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 16;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }
}

/* Model terminate function */
void lab2_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: lab2/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(lab2_DWork.HILInitialize_Card);
    hil_monitor_stop_all(lab2_DWork.HILInitialize_Card);
    is_switching = false;
    if ((lab2_P.HILInitialize_AOTerminate && !is_switching) ||
        (lab2_P.HILInitialize_AOExit && is_switching)) {
      lab2_DWork.HILInitialize_AOVoltages[0] = lab2_P.HILInitialize_AOFinal;
      lab2_DWork.HILInitialize_AOVoltages[1] = lab2_P.HILInitialize_AOFinal;
      lab2_DWork.HILInitialize_AOVoltages[2] = lab2_P.HILInitialize_AOFinal;
      lab2_DWork.HILInitialize_AOVoltages[3] = lab2_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 4U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(lab2_DWork.HILInitialize_Card,
        lab2_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &lab2_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab2_M, _rt_error_message);
      }
    }

    hil_task_delete_all(lab2_DWork.HILInitialize_Card);
    hil_monitor_delete_all(lab2_DWork.HILInitialize_Card);
    hil_close(lab2_DWork.HILInitialize_Card);
    lab2_DWork.HILInitialize_Card = NULL;
  }
}

/*========================================================================*
 * Start of GRT compatible call interface                                 *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  lab2_output(tid);
}

void MdlUpdate(int_T tid)
{
  lab2_update(tid);
}

void MdlInitializeSizes(void)
{
  lab2_M->Sizes.numContStates = (0);   /* Number of continuous states */
  lab2_M->Sizes.numY = (0);            /* Number of model outputs */
  lab2_M->Sizes.numU = (0);            /* Number of model inputs */
  lab2_M->Sizes.sysDirFeedThru = (0);  /* The model is not direct feedthrough */
  lab2_M->Sizes.numSampTimes = (2);    /* Number of sample times */
  lab2_M->Sizes.numBlocks = (29);      /* Number of blocks */
  lab2_M->Sizes.numBlockIO = (11);     /* Number of block outputs */
  lab2_M->Sizes.numBlockPrms = (96);   /* Sum of parameter "widths" */
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
  /* InitializeConditions for UnitDelay: '<S1>/UD' */
  lab2_DWork.UD_DSTATE = lab2_P.UD_X0;

  /* InitializeConditions for DiscreteFir: '<Root>/Discrete FIR Filter' */
  lab2_DWork.DiscreteFIRFilter_states[0] =
    lab2_P.DiscreteFIRFilter_InitialStates;
  lab2_DWork.DiscreteFIRFilter_states[1] =
    lab2_P.DiscreteFIRFilter_InitialStates;
  lab2_DWork.DiscreteFIRFilter_states[2] =
    lab2_P.DiscreteFIRFilter_InitialStates;
  lab2_DWork.DiscreteFIRFilter_states[3] =
    lab2_P.DiscreteFIRFilter_InitialStates;
  lab2_DWork.DiscreteFIRFilter_circBuf = 0;
}

void MdlStart(void)
{
  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: lab2/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q4", "0", &lab2_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab2_M, _rt_error_message);
      return;
    }

    is_switching = false;
    if ((lab2_P.HILInitialize_CKPStart && !is_switching) ||
        (lab2_P.HILInitialize_CKPEnter && is_switching)) {
      result = hil_set_clock_mode(lab2_DWork.HILInitialize_Card, (t_clock *)
        lab2_P.HILInitialize_CKChannels, 2U, (t_clock_mode *)
        lab2_P.HILInitialize_CKModes);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab2_M, _rt_error_message);
        return;
      }
    }

    result = hil_watchdog_clear(lab2_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab2_M, _rt_error_message);
      return;
    }

    if ((lab2_P.HILInitialize_AIPStart && !is_switching) ||
        (lab2_P.HILInitialize_AIPEnter && is_switching)) {
      lab2_DWork.HILInitialize_AIMinimums[0] = lab2_P.HILInitialize_AILow;
      lab2_DWork.HILInitialize_AIMinimums[1] = lab2_P.HILInitialize_AILow;
      lab2_DWork.HILInitialize_AIMinimums[2] = lab2_P.HILInitialize_AILow;
      lab2_DWork.HILInitialize_AIMinimums[3] = lab2_P.HILInitialize_AILow;
      lab2_DWork.HILInitialize_AIMaximums[0] = lab2_P.HILInitialize_AIHigh;
      lab2_DWork.HILInitialize_AIMaximums[1] = lab2_P.HILInitialize_AIHigh;
      lab2_DWork.HILInitialize_AIMaximums[2] = lab2_P.HILInitialize_AIHigh;
      lab2_DWork.HILInitialize_AIMaximums[3] = lab2_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(lab2_DWork.HILInitialize_Card,
        lab2_P.HILInitialize_AIChannels, 4U,
        &lab2_DWork.HILInitialize_AIMinimums[0],
        &lab2_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab2_M, _rt_error_message);
        return;
      }
    }

    if ((lab2_P.HILInitialize_AOPStart && !is_switching) ||
        (lab2_P.HILInitialize_AOPEnter && is_switching)) {
      lab2_DWork.HILInitialize_AOMinimums[0] = lab2_P.HILInitialize_AOLow;
      lab2_DWork.HILInitialize_AOMinimums[1] = lab2_P.HILInitialize_AOLow;
      lab2_DWork.HILInitialize_AOMinimums[2] = lab2_P.HILInitialize_AOLow;
      lab2_DWork.HILInitialize_AOMinimums[3] = lab2_P.HILInitialize_AOLow;
      lab2_DWork.HILInitialize_AOMaximums[0] = lab2_P.HILInitialize_AOHigh;
      lab2_DWork.HILInitialize_AOMaximums[1] = lab2_P.HILInitialize_AOHigh;
      lab2_DWork.HILInitialize_AOMaximums[2] = lab2_P.HILInitialize_AOHigh;
      lab2_DWork.HILInitialize_AOMaximums[3] = lab2_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges(lab2_DWork.HILInitialize_Card,
        lab2_P.HILInitialize_AOChannels, 4U,
        &lab2_DWork.HILInitialize_AOMinimums[0],
        &lab2_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab2_M, _rt_error_message);
        return;
      }
    }

    if ((lab2_P.HILInitialize_AOStart && !is_switching) ||
        (lab2_P.HILInitialize_AOEnter && is_switching)) {
      lab2_DWork.HILInitialize_AOVoltages[0] = lab2_P.HILInitialize_AOInitial;
      lab2_DWork.HILInitialize_AOVoltages[1] = lab2_P.HILInitialize_AOInitial;
      lab2_DWork.HILInitialize_AOVoltages[2] = lab2_P.HILInitialize_AOInitial;
      lab2_DWork.HILInitialize_AOVoltages[3] = lab2_P.HILInitialize_AOInitial;
      result = hil_write_analog(lab2_DWork.HILInitialize_Card,
        lab2_P.HILInitialize_AOChannels, 4U,
        &lab2_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab2_M, _rt_error_message);
        return;
      }
    }

    if (lab2_P.HILInitialize_AOReset) {
      lab2_DWork.HILInitialize_AOVoltages[0] = lab2_P.HILInitialize_AOWatchdog;
      lab2_DWork.HILInitialize_AOVoltages[1] = lab2_P.HILInitialize_AOWatchdog;
      lab2_DWork.HILInitialize_AOVoltages[2] = lab2_P.HILInitialize_AOWatchdog;
      lab2_DWork.HILInitialize_AOVoltages[3] = lab2_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (lab2_DWork.HILInitialize_Card, lab2_P.HILInitialize_AOChannels, 4U,
         &lab2_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab2_M, _rt_error_message);
        return;
      }
    }

    if ((lab2_P.HILInitialize_EIPStart && !is_switching) ||
        (lab2_P.HILInitialize_EIPEnter && is_switching)) {
      lab2_DWork.HILInitialize_QuadratureModes[0] =
        lab2_P.HILInitialize_EIQuadrature;
      lab2_DWork.HILInitialize_QuadratureModes[1] =
        lab2_P.HILInitialize_EIQuadrature;
      lab2_DWork.HILInitialize_QuadratureModes[2] =
        lab2_P.HILInitialize_EIQuadrature;
      lab2_DWork.HILInitialize_QuadratureModes[3] =
        lab2_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode(lab2_DWork.HILInitialize_Card,
        lab2_P.HILInitialize_EIChannels, 4U, (t_encoder_quadrature_mode *)
        &lab2_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab2_M, _rt_error_message);
        return;
      }

      lab2_DWork.HILInitialize_FilterFrequency[0] =
        lab2_P.HILInitialize_EIFrequency;
      lab2_DWork.HILInitialize_FilterFrequency[1] =
        lab2_P.HILInitialize_EIFrequency;
      lab2_DWork.HILInitialize_FilterFrequency[2] =
        lab2_P.HILInitialize_EIFrequency;
      lab2_DWork.HILInitialize_FilterFrequency[3] =
        lab2_P.HILInitialize_EIFrequency;
      result = hil_set_encoder_filter_frequency(lab2_DWork.HILInitialize_Card,
        lab2_P.HILInitialize_EIChannels, 4U,
        &lab2_DWork.HILInitialize_FilterFrequency[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab2_M, _rt_error_message);
        return;
      }
    }

    if ((lab2_P.HILInitialize_EIStart && !is_switching) ||
        (lab2_P.HILInitialize_EIEnter && is_switching)) {
      lab2_DWork.HILInitialize_InitialEICounts[0] =
        lab2_P.HILInitialize_EIInitial;
      lab2_DWork.HILInitialize_InitialEICounts[1] =
        lab2_P.HILInitialize_EIInitial;
      lab2_DWork.HILInitialize_InitialEICounts[2] =
        lab2_P.HILInitialize_EIInitial;
      lab2_DWork.HILInitialize_InitialEICounts[3] =
        lab2_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(lab2_DWork.HILInitialize_Card,
        lab2_P.HILInitialize_EIChannels, 4U,
        &lab2_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(lab2_M, _rt_error_message);
        return;
      }
    }
  }

  /* Start for S-Function (hil_read_encoder_timebase_block): '<Root>/HIL Read Encoder Timebase' */

  /* S-Function Block: lab2/HIL Read Encoder Timebase (hil_read_encoder_timebase_block) */
  {
    t_error result;
    result = hil_task_create_encoder_reader(lab2_DWork.HILInitialize_Card,
      lab2_P.HILReadEncoderTimebase_SamplesI,
      &lab2_P.HILReadEncoderTimebase_Channels, 1,
      &lab2_DWork.HILReadEncoderTimebase_Task);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(lab2_M, _rt_error_message);
    }
  }

  MdlInitialize();
}

void MdlTerminate(void)
{
  lab2_terminate();
}

RT_MODEL_lab2 *lab2(void)
{
  lab2_initialize(1);
  return lab2_M;
}

/*========================================================================*
 * End of GRT compatible call interface                                   *
 *========================================================================*/
