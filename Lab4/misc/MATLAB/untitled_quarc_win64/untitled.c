/*
 * untitled.c
 *
 * Code generation for model "untitled.mdl".
 *
 * Model version              : 1.15
 * Simulink Coder version : 8.1 (R2011b) 08-Jul-2011
 * C source code generated on : Fri Oct 05 18:31:13 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#include "untitled.h"
#include "untitled_private.h"
#include "untitled_dt.h"

/* Block signals (auto storage) */
BlockIO_untitled untitled_B;

/* Block states (auto storage) */
D_Work_untitled untitled_DWork;

/* Real-time model */
RT_MODEL_untitled untitled_M_;
RT_MODEL_untitled *const untitled_M = &untitled_M_;

/* Model output function */
void untitled_output(int_T tid)
{
  int32_T cff;
  real_T acc;
  int32_T j;

  /* S-Function (hil_read_encoder_block): '<Root>/HIL Read Encoder' */

  /* S-Function Block: untitled/HIL Read Encoder (hil_read_encoder_block) */
  {
    t_error result = hil_read_encoder(untitled_DWork.HILInitialize_Card,
      &untitled_P.HILReadEncoder_Channels, 1,
      &untitled_DWork.HILReadEncoder_Buffer);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(untitled_M, _rt_error_message);
    } else {
      untitled_B.HILReadEncoder = untitled_DWork.HILReadEncoder_Buffer;
    }
  }

  /* Derivative: '<Root>/Derivative' */
  {
    real_T t = untitled_M->Timing.t[0];
    real_T timeStampA = untitled_DWork.Derivative_RWORK.TimeStampA;
    real_T timeStampB = untitled_DWork.Derivative_RWORK.TimeStampB;
    real_T *lastU = &untitled_DWork.Derivative_RWORK.LastUAtTimeA;
    if (timeStampA >= t && timeStampB >= t) {
      untitled_B.Derivative = 0.0;
    } else {
      real_T deltaT;
      real_T lastTime = timeStampA;
      if (timeStampA < timeStampB) {
        if (timeStampB < t) {
          lastTime = timeStampB;
          lastU = &untitled_DWork.Derivative_RWORK.LastUAtTimeB;
        }
      } else if (timeStampA >= t) {
        lastTime = timeStampB;
        lastU = &untitled_DWork.Derivative_RWORK.LastUAtTimeB;
      }

      deltaT = t - lastTime;
      untitled_B.Derivative = (untitled_B.HILReadEncoder - *lastU++) / deltaT;
    }
  }

  /* DiscreteFir: '<Root>/Discrete FIR Filter' */
  acc = untitled_B.Derivative * untitled_P.DiscreteFIRFilter_Coefficients[0];
  cff = 1;
  for (j = untitled_DWork.DiscreteFIRFilter_circBuf; j < 4; j++) {
    acc += untitled_DWork.DiscreteFIRFilter_states[j] *
      untitled_P.DiscreteFIRFilter_Coefficients[cff];
    cff++;
  }

  for (j = 0; j < untitled_DWork.DiscreteFIRFilter_circBuf; j++) {
    acc += untitled_DWork.DiscreteFIRFilter_states[j] *
      untitled_P.DiscreteFIRFilter_Coefficients[cff];
    cff++;
  }

  untitled_B.DiscreteFIRFilter = acc;

  /* End of DiscreteFir: '<Root>/Discrete FIR Filter' */

  /* S-Function (hil_read_analog_block): '<Root>/HIL Read Analog' */

  /* S-Function Block: untitled/HIL Read Analog (hil_read_analog_block) */
  {
    t_error result = hil_read_analog(untitled_DWork.HILInitialize_Card,
      &untitled_P.HILReadAnalog_Channels, 1,
      &untitled_DWork.HILReadAnalog_Buffer);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(untitled_M, _rt_error_message);
    }

    untitled_B.HILReadAnalog = untitled_DWork.HILReadAnalog_Buffer;
  }

  /* SignalGenerator: '<Root>/Signal Generator' */
  acc = untitled_P.SignalGenerator_Frequency * untitled_M->Timing.t[0];
  if (acc - floor(acc) >= 0.5) {
    untitled_B.SignalGenerator = untitled_P.SignalGenerator_Amplitude;
  } else {
    untitled_B.SignalGenerator = -untitled_P.SignalGenerator_Amplitude;
  }

  /* End of SignalGenerator: '<Root>/Signal Generator' */

  /* S-Function (hil_write_analog_block): '<Root>/HIL Write Analog' */

  /* S-Function Block: untitled/HIL Write Analog (hil_write_analog_block) */
  {
    t_error result;
    result = hil_write_analog(untitled_DWork.HILInitialize_Card,
      &untitled_P.HILWriteAnalog_Channels, 1, &untitled_B.SignalGenerator);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(untitled_M, _rt_error_message);
    }
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model update function */
void untitled_update(int_T tid)
{
  /* Update for Derivative: '<Root>/Derivative' */
  {
    real_T timeStampA = untitled_DWork.Derivative_RWORK.TimeStampA;
    real_T timeStampB = untitled_DWork.Derivative_RWORK.TimeStampB;
    real_T* lastTime = &untitled_DWork.Derivative_RWORK.TimeStampA;
    real_T* lastU = &untitled_DWork.Derivative_RWORK.LastUAtTimeA;
    if (timeStampA != rtInf) {
      if (timeStampB == rtInf) {
        lastTime = &untitled_DWork.Derivative_RWORK.TimeStampB;
        lastU = &untitled_DWork.Derivative_RWORK.LastUAtTimeB;
      } else if (timeStampA >= timeStampB) {
        lastTime = &untitled_DWork.Derivative_RWORK.TimeStampB;
        lastU = &untitled_DWork.Derivative_RWORK.LastUAtTimeB;
      }
    }

    *lastTime = untitled_M->Timing.t[0];
    *lastU++ = untitled_B.HILReadEncoder;
  }

  /* Update for DiscreteFir: '<Root>/Discrete FIR Filter' */
  untitled_DWork.DiscreteFIRFilter_circBuf =
    untitled_DWork.DiscreteFIRFilter_circBuf - 1;
  if (untitled_DWork.DiscreteFIRFilter_circBuf < 0) {
    untitled_DWork.DiscreteFIRFilter_circBuf = 3;
  }

  untitled_DWork.DiscreteFIRFilter_states[untitled_DWork.DiscreteFIRFilter_circBuf]
    = untitled_B.Derivative;

  /* End of Update for DiscreteFir: '<Root>/Discrete FIR Filter' */

  /* Update absolute time for base rate */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++untitled_M->Timing.clockTick0)) {
    ++untitled_M->Timing.clockTickH0;
  }

  untitled_M->Timing.t[0] = untitled_M->Timing.clockTick0 *
    untitled_M->Timing.stepSize0 + untitled_M->Timing.clockTickH0 *
    untitled_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.002s, 0.0s] */
    /* The "clockTick1" counts the number of times the code of this task has
     * been executed. The absolute time is the multiplication of "clockTick1"
     * and "Timing.stepSize1". Size of "clockTick1" ensures timer will not
     * overflow during the application lifespan selected.
     * Timer of this task consists of two 32 bit unsigned integers.
     * The two integers represent the low bits Timing.clockTick1 and the high bits
     * Timing.clockTickH1. When the low bit overflows to 0, the high bits increment.
     */
    if (!(++untitled_M->Timing.clockTick1)) {
      ++untitled_M->Timing.clockTickH1;
    }

    untitled_M->Timing.t[1] = untitled_M->Timing.clockTick1 *
      untitled_M->Timing.stepSize1 + untitled_M->Timing.clockTickH1 *
      untitled_M->Timing.stepSize1 * 4294967296.0;
  }

  /* tid is required for a uniform function interface.
   * Argument tid is not used in the function. */
  UNUSED_PARAMETER(tid);
}

/* Model initialize function */
void untitled_initialize(boolean_T firstTime)
{
  (void)firstTime;

  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* initialize real-time model */
  (void) memset((void *)untitled_M, 0,
                sizeof(RT_MODEL_untitled));

  {
    /* Setup solver object */
    rtsiSetSimTimeStepPtr(&untitled_M->solverInfo,
                          &untitled_M->Timing.simTimeStep);
    rtsiSetTPtr(&untitled_M->solverInfo, &rtmGetTPtr(untitled_M));
    rtsiSetStepSizePtr(&untitled_M->solverInfo, &untitled_M->Timing.stepSize0);
    rtsiSetErrorStatusPtr(&untitled_M->solverInfo, (&rtmGetErrorStatus
      (untitled_M)));
    rtsiSetRTModelPtr(&untitled_M->solverInfo, untitled_M);
  }

  rtsiSetSimTimeStep(&untitled_M->solverInfo, MAJOR_TIME_STEP);
  rtsiSetSolverName(&untitled_M->solverInfo,"FixedStepDiscrete");

  /* Initialize timing info */
  {
    int_T *mdlTsMap = untitled_M->Timing.sampleTimeTaskIDArray;
    mdlTsMap[0] = 0;
    mdlTsMap[1] = 1;
    untitled_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
    untitled_M->Timing.sampleTimes = (&untitled_M->Timing.sampleTimesArray[0]);
    untitled_M->Timing.offsetTimes = (&untitled_M->Timing.offsetTimesArray[0]);

    /* task periods */
    untitled_M->Timing.sampleTimes[0] = (0.0);
    untitled_M->Timing.sampleTimes[1] = (0.002);

    /* task offsets */
    untitled_M->Timing.offsetTimes[0] = (0.0);
    untitled_M->Timing.offsetTimes[1] = (0.0);
  }

  rtmSetTPtr(untitled_M, &untitled_M->Timing.tArray[0]);

  {
    int_T *mdlSampleHits = untitled_M->Timing.sampleHitArray;
    mdlSampleHits[0] = 1;
    mdlSampleHits[1] = 1;
    untitled_M->Timing.sampleHits = (&mdlSampleHits[0]);
  }

  rtmSetTFinal(untitled_M, -1);
  untitled_M->Timing.stepSize0 = 0.002;
  untitled_M->Timing.stepSize1 = 0.002;

  /* external mode info */
  untitled_M->Sizes.checksums[0] = (143096099U);
  untitled_M->Sizes.checksums[1] = (81688635U);
  untitled_M->Sizes.checksums[2] = (2524818630U);
  untitled_M->Sizes.checksums[3] = (333933851U);

  {
    static const sysRanDType rtAlwaysEnabled = SUBSYS_RAN_BC_ENABLE;
    static RTWExtModeInfo rt_ExtModeInfo;
    static const sysRanDType *systemRan[1];
    untitled_M->extModeInfo = (&rt_ExtModeInfo);
    rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, systemRan);
    systemRan[0] = &rtAlwaysEnabled;
    rteiSetModelMappingInfoPtr(untitled_M->extModeInfo,
      &untitled_M->SpecialInfo.mappingInfo);
    rteiSetChecksumsPtr(untitled_M->extModeInfo, untitled_M->Sizes.checksums);
    rteiSetTPtr(untitled_M->extModeInfo, rtmGetTPtr(untitled_M));
  }

  untitled_M->solverInfoPtr = (&untitled_M->solverInfo);
  untitled_M->Timing.stepSize = (0.002);
  rtsiSetFixedStepSize(&untitled_M->solverInfo, 0.002);
  rtsiSetSolverMode(&untitled_M->solverInfo, SOLVER_MODE_SINGLETASKING);

  /* block I/O */
  untitled_M->ModelData.blockIO = ((void *) &untitled_B);
  (void) memset(((void *) &untitled_B), 0,
                sizeof(BlockIO_untitled));

  /* parameters */
  untitled_M->ModelData.defaultParam = ((real_T *)&untitled_P);

  /* states (dwork) */
  untitled_M->Work.dwork = ((void *) &untitled_DWork);
  (void) memset((void *)&untitled_DWork, 0,
                sizeof(D_Work_untitled));

  /* data type transition information */
  {
    static DataTypeTransInfo dtInfo;
    (void) memset((char_T *) &dtInfo, 0,
                  sizeof(dtInfo));
    untitled_M->SpecialInfo.mappingInfo = (&dtInfo);
    dtInfo.numDataTypes = 15;
    dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
    dtInfo.dataTypeNames = &rtDataTypeNames[0];

    /* Block I/O transition table */
    dtInfo.B = &rtBTransTable;

    /* Parameters transition table */
    dtInfo.P = &rtPTransTable;
  }
}

/* Model terminate function */
void untitled_terminate(void)
{
  /* Terminate for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: untitled/HIL Initialize (hil_initialize_block) */
  {
    t_boolean is_switching;
    t_int result;
    t_uint32 num_final_analog_outputs = 0;
    hil_task_stop_all(untitled_DWork.HILInitialize_Card);
    hil_monitor_stop_all(untitled_DWork.HILInitialize_Card);
    is_switching = false;
    if ((untitled_P.HILInitialize_AOTerminate && !is_switching) ||
        (untitled_P.HILInitialize_AOExit && is_switching)) {
      untitled_DWork.HILInitialize_AOVoltages[0] =
        untitled_P.HILInitialize_AOFinal;
      untitled_DWork.HILInitialize_AOVoltages[1] =
        untitled_P.HILInitialize_AOFinal;
      untitled_DWork.HILInitialize_AOVoltages[2] =
        untitled_P.HILInitialize_AOFinal;
      untitled_DWork.HILInitialize_AOVoltages[3] =
        untitled_P.HILInitialize_AOFinal;
      num_final_analog_outputs = 4U;
    }

    if (num_final_analog_outputs > 0) {
      result = hil_write_analog(untitled_DWork.HILInitialize_Card,
        untitled_P.HILInitialize_AOChannels, num_final_analog_outputs,
        &untitled_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled_M, _rt_error_message);
      }
    }

    hil_task_delete_all(untitled_DWork.HILInitialize_Card);
    hil_monitor_delete_all(untitled_DWork.HILInitialize_Card);
    hil_close(untitled_DWork.HILInitialize_Card);
    untitled_DWork.HILInitialize_Card = NULL;
  }
}

/*========================================================================*
 * Start of GRT compatible call interface                                 *
 *========================================================================*/
void MdlOutputs(int_T tid)
{
  untitled_output(tid);
}

void MdlUpdate(int_T tid)
{
  untitled_update(tid);
}

void MdlInitializeSizes(void)
{
  untitled_M->Sizes.numContStates = (0);/* Number of continuous states */
  untitled_M->Sizes.numY = (0);        /* Number of model outputs */
  untitled_M->Sizes.numU = (0);        /* Number of model inputs */
  untitled_M->Sizes.sysDirFeedThru = (0);/* The model is not direct feedthrough */
  untitled_M->Sizes.numSampTimes = (2);/* Number of sample times */
  untitled_M->Sizes.numBlocks = (15);  /* Number of blocks */
  untitled_M->Sizes.numBlockIO = (5);  /* Number of block outputs */
  untitled_M->Sizes.numBlockPrms = (84);/* Sum of parameter "widths" */
}

void MdlInitializeSampleTimes(void)
{
}

void MdlInitialize(void)
{
  /* InitializeConditions for Derivative: '<Root>/Derivative' */
  untitled_DWork.Derivative_RWORK.TimeStampA = rtInf;
  untitled_DWork.Derivative_RWORK.TimeStampB = rtInf;

  /* InitializeConditions for DiscreteFir: '<Root>/Discrete FIR Filter' */
  untitled_DWork.DiscreteFIRFilter_states[0] =
    untitled_P.DiscreteFIRFilter_InitialStates;
  untitled_DWork.DiscreteFIRFilter_states[1] =
    untitled_P.DiscreteFIRFilter_InitialStates;
  untitled_DWork.DiscreteFIRFilter_states[2] =
    untitled_P.DiscreteFIRFilter_InitialStates;
  untitled_DWork.DiscreteFIRFilter_states[3] =
    untitled_P.DiscreteFIRFilter_InitialStates;
  untitled_DWork.DiscreteFIRFilter_circBuf = 0;
}

void MdlStart(void)
{
  /* Start for S-Function (hil_initialize_block): '<Root>/HIL Initialize' */

  /* S-Function Block: untitled/HIL Initialize (hil_initialize_block) */
  {
    t_int result;
    t_boolean is_switching;
    result = hil_open("q4", "0", &untitled_DWork.HILInitialize_Card);
    if (result < 0) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(untitled_M, _rt_error_message);
      return;
    }

    is_switching = false;
    if ((untitled_P.HILInitialize_CKPStart && !is_switching) ||
        (untitled_P.HILInitialize_CKPEnter && is_switching)) {
      result = hil_set_clock_mode(untitled_DWork.HILInitialize_Card, (t_clock *)
        untitled_P.HILInitialize_CKChannels, 2U, (t_clock_mode *)
        untitled_P.HILInitialize_CKModes);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled_M, _rt_error_message);
        return;
      }
    }

    result = hil_watchdog_clear(untitled_DWork.HILInitialize_Card);
    if (result < 0 && result != -QERR_HIL_WATCHDOG_CLEAR) {
      msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
        (_rt_error_message));
      rtmSetErrorStatus(untitled_M, _rt_error_message);
      return;
    }

    if ((untitled_P.HILInitialize_AIPStart && !is_switching) ||
        (untitled_P.HILInitialize_AIPEnter && is_switching)) {
      untitled_DWork.HILInitialize_AIMinimums[0] =
        untitled_P.HILInitialize_AILow;
      untitled_DWork.HILInitialize_AIMinimums[1] =
        untitled_P.HILInitialize_AILow;
      untitled_DWork.HILInitialize_AIMinimums[2] =
        untitled_P.HILInitialize_AILow;
      untitled_DWork.HILInitialize_AIMinimums[3] =
        untitled_P.HILInitialize_AILow;
      untitled_DWork.HILInitialize_AIMaximums[0] =
        untitled_P.HILInitialize_AIHigh;
      untitled_DWork.HILInitialize_AIMaximums[1] =
        untitled_P.HILInitialize_AIHigh;
      untitled_DWork.HILInitialize_AIMaximums[2] =
        untitled_P.HILInitialize_AIHigh;
      untitled_DWork.HILInitialize_AIMaximums[3] =
        untitled_P.HILInitialize_AIHigh;
      result = hil_set_analog_input_ranges(untitled_DWork.HILInitialize_Card,
        untitled_P.HILInitialize_AIChannels, 4U,
        &untitled_DWork.HILInitialize_AIMinimums[0],
        &untitled_DWork.HILInitialize_AIMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled_M, _rt_error_message);
        return;
      }
    }

    if ((untitled_P.HILInitialize_AOPStart && !is_switching) ||
        (untitled_P.HILInitialize_AOPEnter && is_switching)) {
      untitled_DWork.HILInitialize_AOMinimums[0] =
        untitled_P.HILInitialize_AOLow;
      untitled_DWork.HILInitialize_AOMinimums[1] =
        untitled_P.HILInitialize_AOLow;
      untitled_DWork.HILInitialize_AOMinimums[2] =
        untitled_P.HILInitialize_AOLow;
      untitled_DWork.HILInitialize_AOMinimums[3] =
        untitled_P.HILInitialize_AOLow;
      untitled_DWork.HILInitialize_AOMaximums[0] =
        untitled_P.HILInitialize_AOHigh;
      untitled_DWork.HILInitialize_AOMaximums[1] =
        untitled_P.HILInitialize_AOHigh;
      untitled_DWork.HILInitialize_AOMaximums[2] =
        untitled_P.HILInitialize_AOHigh;
      untitled_DWork.HILInitialize_AOMaximums[3] =
        untitled_P.HILInitialize_AOHigh;
      result = hil_set_analog_output_ranges(untitled_DWork.HILInitialize_Card,
        untitled_P.HILInitialize_AOChannels, 4U,
        &untitled_DWork.HILInitialize_AOMinimums[0],
        &untitled_DWork.HILInitialize_AOMaximums[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled_M, _rt_error_message);
        return;
      }
    }

    if ((untitled_P.HILInitialize_AOStart && !is_switching) ||
        (untitled_P.HILInitialize_AOEnter && is_switching)) {
      untitled_DWork.HILInitialize_AOVoltages[0] =
        untitled_P.HILInitialize_AOInitial;
      untitled_DWork.HILInitialize_AOVoltages[1] =
        untitled_P.HILInitialize_AOInitial;
      untitled_DWork.HILInitialize_AOVoltages[2] =
        untitled_P.HILInitialize_AOInitial;
      untitled_DWork.HILInitialize_AOVoltages[3] =
        untitled_P.HILInitialize_AOInitial;
      result = hil_write_analog(untitled_DWork.HILInitialize_Card,
        untitled_P.HILInitialize_AOChannels, 4U,
        &untitled_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled_M, _rt_error_message);
        return;
      }
    }

    if (untitled_P.HILInitialize_AOReset) {
      untitled_DWork.HILInitialize_AOVoltages[0] =
        untitled_P.HILInitialize_AOWatchdog;
      untitled_DWork.HILInitialize_AOVoltages[1] =
        untitled_P.HILInitialize_AOWatchdog;
      untitled_DWork.HILInitialize_AOVoltages[2] =
        untitled_P.HILInitialize_AOWatchdog;
      untitled_DWork.HILInitialize_AOVoltages[3] =
        untitled_P.HILInitialize_AOWatchdog;
      result = hil_watchdog_set_analog_expiration_state
        (untitled_DWork.HILInitialize_Card, untitled_P.HILInitialize_AOChannels,
         4U, &untitled_DWork.HILInitialize_AOVoltages[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled_M, _rt_error_message);
        return;
      }
    }

    if ((untitled_P.HILInitialize_EIPStart && !is_switching) ||
        (untitled_P.HILInitialize_EIPEnter && is_switching)) {
      untitled_DWork.HILInitialize_QuadratureModes[0] =
        untitled_P.HILInitialize_EIQuadrature;
      untitled_DWork.HILInitialize_QuadratureModes[1] =
        untitled_P.HILInitialize_EIQuadrature;
      untitled_DWork.HILInitialize_QuadratureModes[2] =
        untitled_P.HILInitialize_EIQuadrature;
      untitled_DWork.HILInitialize_QuadratureModes[3] =
        untitled_P.HILInitialize_EIQuadrature;
      result = hil_set_encoder_quadrature_mode(untitled_DWork.HILInitialize_Card,
        untitled_P.HILInitialize_EIChannels, 4U, (t_encoder_quadrature_mode *)
        &untitled_DWork.HILInitialize_QuadratureModes[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled_M, _rt_error_message);
        return;
      }

      untitled_DWork.HILInitialize_FilterFrequency[0] =
        untitled_P.HILInitialize_EIFrequency;
      untitled_DWork.HILInitialize_FilterFrequency[1] =
        untitled_P.HILInitialize_EIFrequency;
      untitled_DWork.HILInitialize_FilterFrequency[2] =
        untitled_P.HILInitialize_EIFrequency;
      untitled_DWork.HILInitialize_FilterFrequency[3] =
        untitled_P.HILInitialize_EIFrequency;
      result = hil_set_encoder_filter_frequency
        (untitled_DWork.HILInitialize_Card, untitled_P.HILInitialize_EIChannels,
         4U, &untitled_DWork.HILInitialize_FilterFrequency[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled_M, _rt_error_message);
        return;
      }
    }

    if ((untitled_P.HILInitialize_EIStart && !is_switching) ||
        (untitled_P.HILInitialize_EIEnter && is_switching)) {
      untitled_DWork.HILInitialize_InitialEICounts[0] =
        untitled_P.HILInitialize_EIInitial;
      untitled_DWork.HILInitialize_InitialEICounts[1] =
        untitled_P.HILInitialize_EIInitial;
      untitled_DWork.HILInitialize_InitialEICounts[2] =
        untitled_P.HILInitialize_EIInitial;
      untitled_DWork.HILInitialize_InitialEICounts[3] =
        untitled_P.HILInitialize_EIInitial;
      result = hil_set_encoder_counts(untitled_DWork.HILInitialize_Card,
        untitled_P.HILInitialize_EIChannels, 4U,
        &untitled_DWork.HILInitialize_InitialEICounts[0]);
      if (result < 0) {
        msg_get_error_messageA(NULL, result, _rt_error_message, sizeof
          (_rt_error_message));
        rtmSetErrorStatus(untitled_M, _rt_error_message);
        return;
      }
    }
  }

  MdlInitialize();
}

void MdlTerminate(void)
{
  untitled_terminate();
}

RT_MODEL_untitled *untitled(void)
{
  untitled_initialize(1);
  return untitled_M;
}

/*========================================================================*
 * End of GRT compatible call interface                                   *
 *========================================================================*/
