/*
 * untitled_types.h
 *
 * Code generation for model "untitled.mdl".
 *
 * Model version              : 1.15
 * Simulink Coder version : 8.1 (R2011b) 08-Jul-2011
 * C source code generated on : Fri Oct 05 18:31:13 2018
 *
 * Target selection: quarc_win64.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: 32-bit Generic
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */
#ifndef RTW_HEADER_untitled_types_h_
#define RTW_HEADER_untitled_types_h_
#include "rtwtypes.h"

/* Parameters (auto storage) */
typedef struct Parameters_untitled_ Parameters_untitled;

/* Forward declaration for rtModel */
typedef struct RT_MODEL_untitled RT_MODEL_untitled;

#endif                                 /* RTW_HEADER_untitled_types_h_ */
