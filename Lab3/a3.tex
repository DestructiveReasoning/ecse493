\documentclass[12pt]{report}

\usepackage{amsmath,amssymb,amsfonts}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{bold-extra}
\usepackage{subcaption}
\usepackage{gensymb}
\usepackage[margin=2cm]{geometry}

\hypersetup {
  colorlinks=true,
  linkcolor=blue,
  linktoc=true,
}

\lstset {
  language=Matlab,
  breaklines=true,
  showspaces=false,
  frame=single,
  basicstyle=\footnotesize\ttfamily
}

\title{ECSE 493 - Control and Robotics Lab\\Lab Assignment 3}
\author{Harley Wiltzer\\(260690006)\and Jacob Shnaidman\\(260655643)}
\date{\today}

\begin{document}
\maketitle
\pagenumbering{roman}
\chapter*{An Excogitating Exordium}
\addcontentsline{toc}{chapter}{An Excogitating Exordium}
\begin{flushright}
  \textit{``Hundreds of thousands of babies are born every day.\\While the whole phenomenon is
  menacing,\\one of them by itself is not newsworthy."}\\
  \textit{- Richard Stallman}
\end{flushright}
The text you are about to read examines, explores, and finicks with the world of classical control
systems in ways that have most likely been documented many times before. That is not to say,
however, that nothing is to be learned from this \textit{conlatio}. If nothing else, readers of this
text will catch a glimpse of and learn about how the authors learned throughout the experimentation
process. After all, learning from the mistakes of others has been beneficial to society since the
beginning of time. Regardless of how much of a titan one may be in their industry, there is always
room to learn more -- for instance, Microsoft recently learned that it is not a good idea to
forcefully remove \texttt{/bin/sh} in a *nix install
script\footnote{\url{https://www.theregister.co.uk/2018/06/14/microsoft_r_open_debian_dev}} that
will be distributed to customers.\\\\
The remainder of this text will be organized as follows. Part \ref{p:pid} will go beyond the
analysis of Proportional, Integral, and Derivative controller design. In particular, it will examine
Proportional OR Integral OR Derivative design, where OR signifies a non-exclusive logical conjunction. The
various configurations will then be pitted against each other and stack-ranked. This part of the
text will also present the design of a PID controller that meets harsh criteria for the
1-dimensional position control of a cart. Following this, the \textit{lead-lag} paradigm of control
design will be studied in Part \ref{p:leadlag}. After analysis of the effects of lead and lag
compensators, the lead-lag and Proportional OR Integral OR Derivative paradigms will undergo
thorough comparison. Overall, each control configuration will be supported by pictorial evidence of
performance in both the theoretical and experimental realms.\\\\
The authors wish to remind the readers that the discoveries made throughout these laboratory
experiments present no new scientific breakthroughs -- therefore the authors urge the readers
to refrain from nominating this paper for publication in scientific academic journals. This text will
likely be most effective as a case study for burgeoning engineering students or individuals that are
interested in how others might learn control design by trial and (mostly) error.\\\\
Finally, the authors wish to express their sincere gratitude to the readers of this paper. They are
truly gentlemen, women, and scholars.
\tableofcontents
\listoffigures
\begingroup
\let\clearpage\relax
\listoftables
\lstlistoflistings
\endgroup
\newpage
\pagenumbering{arabic}
\part{Proportional, Integral, and Derivative Control}\label{p:pid}
\chapter*{On the Finagling of PID Parameters}
\addcontentsline{toc}{chapter}{On the Finagling of PID Parameters}
We begin our foray into classical controller design by finagling with the designs of PI, PD, and PID
controllers, where P signifies proportional gain, I signifies integral gain, and D signifies
derivative gain. These controllers were tested by implementing 1-dimensional position controller on
a physical plant in the lab. Wiltzer and Shnaidman previously showed from experimentation the plant has the
following tranfer function $G(s)$ from applied voltage to position:
\begin{equation}\label{eq:g}
  G(s) \approx \frac{3.25}{s^2 + 14.15s}
\end{equation}
The goal is to design a controller of the form
\begin{equation}\label{eq:pid-form}
  H(s) = K_P + K_Ds + \frac{K_I}{s}
\end{equation}
where $K_P,K_I,K_D$ are proportional, integral, and derivative gain respectively. These are
parameters that must be tuned to obtain the requisite control behavior.\\\\

The $K_p$, $K_d$ and $K_i$ parameters were found using the loopshaping method by analyzing the step response and bode plots in Figure \ref{fig:loopshaping_step} and \ref{fig:loopshaping_bode} respectively. These graphs were found using a function shown below in Listing \ref{l:getpid}. The bode plots in Figure \ref{fig:loopshaping_bode} showed that these systems were clearly very stable. The step responses in Figure \ref{fig:loopshaping_step} showed a reasonable starting point to begin testing these parameters with the cart. All figures have 0 steady state error and reasonable rise time. The overshoot in Figure \ref{fig:loopshaping_step} (c) could not be avoided without a derivative controller, however the overshoot is minimal (less than 6\%).

\lstinputlisting[caption={A Matlab function for generating the step response and bode plot given $K_p$, $K_i$, $K_d$},label={l:getpid}]{res/get_pid_resp.m}


\begin{figure}[h]
  \caption{Closed loop step response of PID, PD, and PI Controllers}\label{fig:loopshaping_step}
  \centering
    \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/1_pid_loopshaping_step.jpg}
    \caption{(PID step response ($K_p$=60, $K_i$=5, $K_d$ = 8)}
  \begin{subfigure}[b]{0.4\textwidth}
    \centering
    \includegraphics[scale=0.4]{res/1_pd_loopshaping_step.jpg}
    \caption{PD step response ($K_p$=10, $K_d$ = 3)}
  \end{subfigure}
  \begin{subfigure}[b]{0.40\textwidth}
    \centering
    \includegraphics[scale=0.4]{res/1_pi_loopshaping_step.jpg}
    \caption{PI step response ($K_p$=2.4, $K_i$ = 0.1)}
  \end{subfigure}
  \end{subfigure}
\end{figure}

\begin{figure}[h]
  \caption{Closed loop Bode plot of PID, PD, and PI Controllers}\label{fig:loopshaping_bode}
  \centering
    \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/1_pid_loopshaping_bode.jpg}
    \caption{(PID step response ($K_p$=60, $K_i$=5, $K_d$ = 8)}
  \begin{subfigure}[b]{0.4\textwidth}
    \centering
    \includegraphics[scale=0.4]{res/1_pd_loopshaping_bode.jpg}
    \caption{PD step response ($K_p$=10, $K_d$ = 3)}
  \end{subfigure}
  \begin{subfigure}[b]{0.40\textwidth}
    \centering
    \includegraphics[scale=0.4]{res/1_pi_loopshaping_bode.jpg}
    \caption{PI step response ($K_p$=2.4, $K_i$ = 0.1)}
  \end{subfigure}
  \end{subfigure}
\end{figure}

Unfortunately, the parameters obtained by traditional loop-shaping methodology did not provide
acceptable behavior, as the gains were likely too low to have the right influence on the cart in the
non-ideal laboratory setting. Thus, the authors were left to their own devices, and used an
intricate process of deductive reasoning to deduce how the parameters affect the behavior of the
cart. This allowed for the design of some interesting controllers that have desirable
properties. Ultimately, a tuple $(K_P,K_D,K_I) = (250, 13, 10)$ was chosen as for all PI, PD, and
PID configurations tested with the relevant gains from the aforementioned tuple provided decent
performance -- that is to say, overshoot under 25\%, steady state error under $0.04$, and reasonably
fast rise time. Figures \ref{fig:pd}, \ref{fig:pi}, and \ref{fig:pid} show the experimental step
responses of the systems.\\\\
\begin{figure}[h]
  \caption{PD Controller, $K_P=250, K_D = 13, K_I = 0$}\label{fig:pd}
  \centering
  \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/lab3p1-250_13_0-step}
    \caption{Step response}
  \end{subfigure}
  \begin{subfigure}[b]{0.40\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/lab3p1-250_13_0-ramp-transient}
    \caption{Ramp response}
  \end{subfigure}
  \begin{subfigure}[b]{0.40\textwidth}
    \centering
    \includegraphics[scale=0.9]{res/lab3p1-250_13_0-ramp}
    \caption{Ramp response steady state error (input signal is blue)}
  \end{subfigure}
\end{figure}
\begin{figure}[h]
  \caption{PI Controller, $K_P=250, K_D = 0, K_I = 10$}\label{fig:pi}
  \centering
  \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/lab3p1-250_0_10-step}
    \caption{Step response}
  \end{subfigure}
  \begin{subfigure}[b]{0.40\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/lab3p1-250_0_10-ramp-transient}
    \caption{Ramp response}
  \end{subfigure}
  \begin{subfigure}[b]{0.40\textwidth}
    \centering
    \includegraphics[scale=0.9]{res/lab3p1-250_0_10-ramp}
    \caption{Ramp response steady state error (input signal is blue)}
  \end{subfigure}
\end{figure}
\begin{figure}[h]
  \caption{PID Controller, $K_P=250, K_D = 13, K_I = 10$}\label{fig:pid}
  \centering
  \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/lab3p1-250_13_10-step}
    \caption{Step response}
  \end{subfigure}
  \begin{subfigure}[b]{0.40\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/lab3p1-250_13_10-ramp-transient}
    \caption{Ramp response}
  \end{subfigure}
  \begin{subfigure}[b]{0.40\textwidth}
    \centering
    \includegraphics[scale=0.9]{res/lab3p1-250_13_10-ramp}
    \caption{Ramp response steady state error (input signal is on top)}
  \end{subfigure}
\end{figure}
The authors began by proportionately scaling up the three gain parameters as a baseline and began
finicking with them from there. It was quickly seen that increasing the $K_D$ gain 
increases the rise time and decreases overshoot of the system, while providing no change to steady
state error. However, increasing the $K_I$
parameter tends to increase overshoot and oscillation, but reduces the steady state error of the
response. Thus, the $K_P$ parameter was first chosen to provide a slightly-faster-than-necessary
rise time (which we desired to be $0.3$ seconds). This was achieved with $K_P=250$. Then, $K_D$ was
decreased in order to eliminate overshoot, which occured at $K_D=13$. Finally, $K_I$ was then
increased until the PID steady-state error was minimized, which occured at $K_I = 10$. Beyond this
point, increasing $K_I$ did not have much effect on the elimination of steady-state error -- this is
likely due to error and non-ideality of the plant relative to the theoretical model. Table
\ref{t:pid} summarizes the performance of each of the configurations.
\begin{table}[h]
  \caption{Comparison of experimental PI, PD, and PID controller performance}\label{t:pid}
  \centering
  \scriptsize
  \begin{tabular}{|c|c|c|c|c|c|}
    \hline
    \textbf{Config.} & $(K_P,K_D,K_I)$ & \textbf{Rise Time} & \textbf{Overshoot} &
    \textbf{Steady-State Error (Step)} & \textbf{Steady-State Error (Ramp)}\\\hline
    PD & $(250,13,0)$ & 0.288s & \textbf{0} & 0.08cm & 0.38cm\\\hline
    PI & $(250,0,10)$ & \textbf{0.2s} & 25\% & \textbf{0.01cm} & 0.37cm\\\hline
    PID & $(250,13,10)$ & 0.266s & \textbf{0} & 0.02cm & \textbf{0.35cm}\\\hline
  \end{tabular}
\end{table}
Note the relevance of the ramp response experiment. With a ramp input, the \textit{type} of the
system increases by 1 -- that is to say, the degree of the system's pole at the origin increases by
1. By the final value theorem, this poses significant challenges for reduction of steady state
error, as the denominator of $\lim_{s\to 0}sH(s)G(s)X(s)$ is decreased when the input $X(s)$ is a
ramp, denoted by $1/s$. In accordance with the theory, our experiments show that the addition of
integral gain aids to reduce this steady state error even for ramp inputs. Table \ref{t:pid} shows
that the two controllers with integral gain had the lowest steady state error in response to the
ramp input, which demonstrates the importance of integral gain in controller design. Moreover, note
that the plant transfer function of Eq. (\ref{eq:g}) is of Type 1, due to the $s$ factor on its
denominator. This means that a proportional controller must exhibit steady-state error in response
to a ramp input, even in the theoretical realm, by the Final Value Theorem. Thus, it is a good idea
to check the ramp response to make sure that the compensation by the integral gain provides
acceptable steady-state error.
\chapter*{Designing a PID Controller to Meet Harsh Specifications}
\addcontentsline{toc}{chapter}{Designing a PID Controller to Meet Harsh Specifications}
Next, we attempt to design a PID controller that meets more rigorous specifications than those
above. In particular, the desired performance is characterized by the following criteria for the
step response:
\begin{itemize}
  \item Zero steady-state error
  \item Overshoot $<10\%$
  \item Fastest possible rise time
\end{itemize}
Using the intuition for the effect that $K_P$, $K_I$, and $K_D$ have on the performance of PID
controllers as described above, the design of the desired controller was carried out in a similar
fashion to that of the previous controllers. The first order of business was to determine a gain
$K_P$ such that the rise time of the system ceases to increase non-negligibly. We found that
increasing $K_P$ beyond a value of 1200 had no more considerable impact on the rise time, so this
was the gain that was chosen.\\\\
Following this realization, $K_D$ was tuned to reduce the overshoot of the system below $10\%$. We
began incrementally increasing $K_d$ until the value of $28$, at which point overshoot was seen to
be approximately $8.5\%$. At this point, it was desired to stop increasing $K_D$, otherwise the rise
time would increase unnecessarily.\\\\
Finally, the $K_I$ parameter was tuned to annihilate the steady state error. After incrementally
increasing the $K_I$ gain to $14$, the steady-state error was reduced to $0$. However, the observed
maximum overshoot was still below $10\%$, so $K_I$ was increased more until the overshoot got closer
to $10\%$ in order to reduce the rise time.  With $K_I=17$, the maximum overshoot was observed to be
approximately $9.67\%$, so no more modifications were made.\\\\
To conclude, with $(K_P,K_I,K_D) = (1200,17,28)$, all of the design specifications were met, and the
rise time was seen to be $0.174$ seconds. The observed step response is shown in Figure
\ref{fig:1:d}.
\begin{figure}[h]
  \caption{PID Controller meeting design specifications}\label{fig:1:d}
  \centering
  \includegraphics[scale=0.7]{res/lab3p1d}
\end{figure}
\part{Lead-Lag Control}\label{p:leadlag}
\chapter*{On the Design of a Garden-Variety Lead Controller}
\addcontentsline{toc}{chapter}{On the Design of a Garden-Variety Lead Controller}

We begin our exploration of the lead-lag paradigm of controller design with the attempt to design a
garden-variety lead controller. We set out to design a lead controller with a phase margin of
$60\degree$, while improving upon the rise time and overshoot that would normally be seen with a
proportional controller. The garden-variety lead controller takes the following form:
\begin{equation}\label{eq:lead}
  H_\text{lead}(s) = k_\text{lead}\frac{\alpha\tau_as + 1}{\tau_a - 1}\hspace{1cm}\alpha > 1
\end{equation}
Therefore, the parameters $k_\text{lead}$, $\alpha$, and $\tau_a$ must be chosen. Rather than
choosing $k_\text{lead}$ directly, we chose to take a more intuitive approach and try choosing a
designated cutoff frequency $\omega_c$ as a baseline. Since the lead controller is, at its core, a
zero followed by a pole, it behaves like a high-pass filter with cutoff frequency $\omega_c$
expressed as:
\begin{equation}\label{eq:wc}
  \omega_c = \frac{1}{\sqrt\alpha\tau_a}
\end{equation}
Therefore, varying $\omega_c$ determines the bandwidth of frequencies that are amplified by the
controller, and specifies the region for which phase is increased. Therefore, given some $\omega_c$,
we may design $\alpha$ (and by transitivity, $\tau_a$) by determining the amount of requisite phase
shift due to the lead controller $\hat\phi$. For a plant $G(s)$ and a desired phase margin $\phi$,
we determine $\hat{\phi}$ as follows:
\begin{equation}\label{eq:philead}
  \hat{\phi} = -180\degree - \angle G(j\omega_c) + 60\degree
\end{equation}
Then, by engineering wizardry, the trick of the trade is to set $\alpha$ as follows:
\begin{equation}\label{eq:alpha}
  \alpha = \frac{(1 + \sin\hat\phi)^2}{1-\sin^2\hat\phi}
\end{equation}
Equipped with this knowledge, Listing \ref{l:getlead} was created to derive a lead controller of the
form of Eq. (\ref{eq:lead}) given some $\omega_c$, and Listing \ref{l:genleads} was conceived to
generate lead controllers with a given list of $\omega_c$ parameters and plot all step responses on
the same graph.
\lstinputlisting[caption={A Matlab function for creating lead controllers given
$\omega_c$},label={l:getlead}]{res/get_lead_controller.m}
\lstinputlisting[caption={A Matlab function for generating and evaluating many lead
controller},label={l:genleads}]{res/gen_lead_controllers.m}
Figure \ref{fig:bunch-of-leads} shows the resulting step responses generated by Listing
\ref{l:genleads} with $6$ different values of $\omega_c$ given by \texttt{logspace(1,4,6)}.
\begin{figure}[h]
  \caption{Comparison of Lead Controllers by $\omega_c$}\label{fig:bunch-of-leads}
  \centering
  \includegraphics[scale=0.7]{res/lab3p2lead-multiplew}
\end{figure}
As expected, with higher $\omega_c$, the rise time becomes lower, and the overshoot increases. This
makes sense because with higher $\omega_c$, the controller is amplifying the higher frequencies. We
see that at $\omega_c=39.8107$rad/s, the overshoot of the system is just over $10\%$. Reducing $\omega_c$
to $32$ rad/s puts the maximum overshoot just under $10\%$ while slightly decreasing the rise time. The
resulting bode plot and simulated step response to a $20$cm step input are shown in Figure
\ref{fig:lead:theoretical}.
\begin{figure}[h]
  \caption{Design of Lead Controller with $60\degree$ Phase Margin}\label{fig:lead:theoretical}
  \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/lab3p2leadbode}
    \caption{Bode plot of the loop gain}
  \end{subfigure}
  \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[scale=0.55]{res/lab3p2-leadsteptheoretical}
    \caption{Step response to 20cm input}
  \end{subfigure}
\end{figure}
Note that the rise time of this configuration is approximately $0.06$ seconds, and maximum overshoot
is just under $10\%$. This is an extraordinary improvement over the proportional controller
previously designed by Wiltzer and Shnaidman in lab 2, which is shown in Figure
\ref{fig:lab2_controllers}. Figure (a) has large steady state error and Figure (b) has a very large
overshoot that results in a second oscillation after the first. The best controller from the last
lab is shown in Figure (b), with an overshoot of approximately $16.67\%$ and a rise time of
approximately $0.25$ seconds. Experiments will show if this configuration will prove superior to our
previous design. \emph{Dum spiro, spero.}

\begin{figure}[h]
  \caption{Step response of P controllers from lab 2 with values of $K_p$ = (20,100,200)}\label{fig:lab2_controllers}
  \centering
    \begin{subfigure}[b]{\textwidth}
    \centering
    \includegraphics[scale=0.5]{res/lab2pos_k20.png}
    \caption{(P controller step response with $K_p$=20}
  \begin{subfigure}[b]{0.4\textwidth}
    \centering
    \includegraphics[scale=0.4]{res/lab2pos_k100.png}
    \caption{P controller step response with $K_p$=100}
  \end{subfigure}
  \begin{subfigure}[b]{0.40\textwidth}
    \centering
    \includegraphics[scale=0.4]{res/lab2pos_k200.png}
    \caption{P controller step response with $K_p$=200}
  \end{subfigure}
  \end{subfigure}
\end{figure}

\chapter*{Experimental Performance of the Lead Controller}
\addcontentsline{toc}{chapter}{Experimental Performance of the Lead Controller}
The lead controller with $\omega_c=32$rad/s was realized and applied to the physical plant.
Subsequently, the step response to a $15$cm step input is shown in Figure
\ref{fig:lead:experimental}. Note that in the physical realization, no overshoot is observed -- this
is likely due to damping caused by non-idealities that aren't accounted for in the plant model.
Consequently, the observed rise time is relatively decreased as well, and is noted to be
approximately $0.258$ seconds. This is an objective improvement over the best proportional
controller designed previously by Wiltzer and Shnaidman, which had approximately the same rise time,
but a maximum overshoot of almost $20\%$.
\begin{figure}[h]
  \caption{Experimental Performance of the Lead Controller with
  $\omega_c=32$rad/s}\label{fig:lead:experimental}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.58]{res/lab3p2leadstep}
    \caption{Step response with 15cm step}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.58]{res/lab3p2leadramp}
    \caption{Ramp response (input signal on top)}
  \end{subfigure}
\end{figure}
\chapter*{Adding a Lag Compensator to the Lead Controller of Yesteryear}
\addcontentsline{toc}{chapter}{Adding a Lag Compensator to the Lead Controller of Yesteryear}
Although the lead controller shown previously may seem miraculous, do not be swooned just yet: the
lead controller may exhibit less-than-humorous steady-state error to ramp inputs. Thus, the authors
wish to add a lag compensator that will decrease the steady-state error in response to a ramp input
by at least a factor of $10$. To commence, let us examine the steady-state error of the lead
controller when faced with a ramp input, as shown in Figure \ref{fig:lead:ramp:theoretical}.
\begin{figure}[h]
  \caption{Steady-State Error of Lead Controller Undergoing Ramp
  Input}\label{fig:lead:ramp:theoretical}
  \centering
  \includegraphics[scale=0.6]{res/lab3p2leadramptheoretical}\\
  \textit{Input signal is on top}
\end{figure}
Note that the observed steady state error here is slightly greater than $0.02\%$.\\\\
A lag compensator can be thought of as a lead compensator's evil cousin -- although some may argue
that it isn't even evil. A lag compensator, therefore, is essentially a lead compensator's cousin, or
perhaps its fraternal twin.\\\\
In \textit{dualitatem} to the lead compensator, the lag compensator
acts as a low-pass filter with cutoff frequency $\omega_s$ and tends to decrease phase over an
interval. Its transfer function assumes the form:
\begin{equation}\label{eq:lag}
  H_\text{lag}(s) = \frac{1}{\beta}\frac{\beta\tau_bs+1}{\tau_b+1}\hspace{1cm}\beta\in(0,1)
\end{equation}
The principal purpose of the lag compensator is to decrease steady-state error, and therefore to
decrease the sensitivity of the system. We define sensitivity $\mathcal{S}$ as follows:
\begin{equation}\label{eq:sensitivity}
  \mathcal{S} > \frac{1}{1+\frac{1}{\beta}H_\text{lead}(j\omega_s)G(j\omega_s)}
\end{equation}
where $\omega_s$ is the cutoff frequency of the lag compensator seen as a low-pass filter, and
$\frac{1}{\beta}$ is the gain of the lag compensator. If we define $\mathcal{S}$ to be less than a
tenth of the ramp steady state error observed with the lead compensator, and $\omega_s$ is chosen
well, the effective lead-lag controller will meet the steady-state error criterion. Moreover, an
engineering trick of the trade tells us to choose $\tau_b=1/\omega_s$. Therefore, it is sufficient
to choose $\omega_s$ in order to design a lag controller.\\\\
When choosing $\omega_s$, it is important that the region where the phase is shifted in the lag
controller's open loop bode plot does not interfere with the region where the lead controller
exhibits a frequency shift. When this happens, the system's phase margin may be compromised.
Therefore, to keep the lowest possible rise time while decreasing the steady state error, it is
desired to choose the largest $\omega_s$ that avoids this event.\\\\
Armed with this knowledge, the code of Listing \ref{l:getlag} was concocted to generate Matlab lag
controller models.
\lstinputlisting[caption={Code for generating Lag
controllers},label={l:getlag}]{res/get_lag_controller.m}
We choose sensitivity $\mathcal{S}=0.0002$, which is a tenth of the steady-state error demonstrated by
the garden-variety lead controller. Moreover, by iteratively choosing $\omega_s$ values, we found
that $\omega_s=0.05$rad/s was toward the brink of interference with the phase shift due to the lead
controller. Using these parameters, we use the function in Listing \ref{l:getlag} to create a lag
controller. The open loop bode plot of the overall system
$H_\text{lag}(j\omega)H_\text{lead}(j\omega)G(j\omega)$ is shown in Figure \ref{fig:leadlag:bode}.
Moreover, the corresponding response to a $20$cm step input is shown in Figure
\ref{fig:leadlag:step:theoretical}.
\begin{figure}[h]
  \caption{Bode Plot of Open Loop Lead-Lag System}\label{fig:leadlag:bode}
  \centering
  \includegraphics[scale=0.6]{res/lab3p2leadlagbode}
\end{figure}
\begin{figure}[h]
  \caption{Step Response of Lead-Lag System with 20cm Step}\label{fig:leadlag:step:theoretical}
  \centering
  \includegraphics[scale=0.6]{res/lab3p2leadlagsteptheoretical}
\end{figure}
Note that the step response of the lead-lag system demonstrates a slight increase in rise time. The
intent is for this slight performance decrease to trade for improved steady-state error in response
to a ramp input. We observe the response to a ramp input in Figure
\ref{fig:leadlag:ramp:theoretical}.
\begin{figure}[h]
  \caption{Ramp Response of Lead-Lag System}\label{fig:leadlag:ramp:theoretical}
  \centering
  \includegraphics[scale=0.6]{res/lab3p2leadlagramptheoretical}
\end{figure}
Note that the demonstrated steady-state error shown in Figure \ref{fig:leadlag:ramp:theoretical} is
 $0.0002\%$ -- which in fact is better than a tenfold improvement over the garden-variety lead
 controller in that regard.

\chapter*{Experimental Observations of the Lead-Lag Controller}
\addcontentsline{toc}{chapter}{Experimental Observations of the Lead-Lag Controller}
The lead-lag controller designed previously was realized in Simulink and tested on the physical
plant. The observed step response and ramp response are shown in Figure
\ref{fig:leadlag:experimental}.
\begin{figure}[h]
  \caption{Experimental Behavior of the Lead-Lag Controller}\label{fig:leadlag:experimental}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.58]{res/lab3p2leadlagstep}
    \caption{Step response with 15cm step}
  \end{subfigure}
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \includegraphics[scale=0.58]{res/lab3p2leadlagramp}\\
    \caption{Ramp response (input signal on top)}
  \end{subfigure}
\end{figure}
Once again, non-ideal damping was exhibited, causing the observed response to have no overshoot and
a slower rise time compared to the theoretical responses. Unfortunately, the steady-state error upon
a ramp input managed to be slightly worse with the leadlag controller in the physical
implementation, at 0.678cm compared to 0.46cm from the garden-variety lead controller. However, the
authors attribute this discrepancy to the aforementioned non-idealities that exist in the physical
plant that aren't considered in the modelling, such as friction on the cart and the motor, as well
as tension exerted by the wires connecting the plant to the computer. Moreover, the leadlag
controller exhibited a rise time of 0.27s, compared to the 0.258s of the lead controller. Neither
controller showed any overshoot. We conclude this text with a comparison of all controllers designed
throughout the experiments discussed in the section below.
\section*{Comparison of Controller Performance}
The differences in performance between all of the discussed controllers are summarized in Table
\ref{t:comparison}.
\begin{table}[h]
  \caption{Comparison of experimental controller performance}\label{t:comparison}
  \centering
  \scriptsize
  \begin{tabular}{|c|c|c|c|c|}
    \hline
    \textbf{Config.} & \textbf{Rise Time} & \textbf{Overshoot} & \textbf{Steady-State Error (Step)}
    & \textbf{Steady-State Error (Ramp)}\\\hline
    PD & 0.288s & \textbf{0} & 0.08cm & 0.38cm\\\hline
    PI & \textbf{0.2s} & 25\% & \textbf{0.01cm} & 0.37cm\\\hline
    PID & 0.266s & \textbf{0} & 0.02cm & \textbf{0.35cm}\\\hline
    Lead & 0.258s & \textbf{0} & 0.16cm & 0.46cm\\\hline
    Lead-Lag & 0.270s & \textbf{0} & 0.14cm & 0.678cm\\\hline
  \end{tabular}
\end{table}
Based on these results, the performance of the lead and lead-lag controllers is
apparently quite underwhelming. Both the lead and the lead-lag controllers exhibited far worse
steady-state error on the ramp input as well as the step input. One reason that may
explain this is that the PI and PID controllers have the advantage of directly adding a pole
at the origin, which makes manipulating the steady-state error very easy and direct. This is not the case
in general for lead or lead-lag controllers, which could explain their relative inability to reduce
steady-state error. Moreover, the lead and lead-lag controllers could not improve upon the rise time
of the PI controller (though keep in mind, this controller had terrible overshoot), and only the
lead controller barely outperformed the PID controller. However, there is a much more omnipotent
reason for the lead and lead-lag controllers' lack of competence, and that is how they were designed
compared to the other controllers.\\\\
When designing the lead and lead-lag controllers, we started with a theoretical model and simply
applied this theoretical model to the physical plant. However, for the PI, PD, and PID controllers,
we started experimentation directly on the plant and tuned parameters as we saw fit. This
effectively eliminates any model error from the physical plant during the design! Instead of picking
parameters that optimize the controllers in the ideal case, the PI, PD, and PID parameters were
directly chosen to optimize the performance on the physical model. Therefore, it is no surprise that
the lead and lead-lag controllers fell short.\\\\
With this in mind, we may discuss the relative merits of the lead and lead-lag controllers. To
begin, the authors found that the lead and lead-lag controllers had a more intuitive interpretation
of parameters, which made optimizing the controllers (at least in the theoretical realm) much more
trivial. By interpreting the parameters as cutoff frequencies of filters, it was fairly easy to
design the controllers in the frequency domain -- especially relative to the PI, PD, and PID
controllers. This manifests itself in the incredible theoretical results, and in the physical
realization of the lead controller. The physical lead controller had the quickest rise time and
\textit{showed no overshoot}. Therefore, the benefit of the lead-lag controller design, from the
perspective of the authors, is how interpretable the parameters are. The three parameters,
$\omega_c,\omega_s,\mathcal{S}$ can be linked to direct physical quantites that can be inferred from
simple frequency domain analysis and steady-state error observations, so given some concrete
constraints, the determination of the necessary parameters is very clear.\\\\
To conclude, we summarize the relative merits of each controller. Both the PI and lead controllers
were shown to perform well if rise time is the number one priority. The PI controller showed the
fastest rise time, however it was far easier to control the overshoot with the lead controller. The
PD controller is good when overshoot is most-feared and stability is most fragile, while rise time
and steady-state error are not of high importance. Finally, the PID and lead-lag controllers tend to
be the ``middle-ground": they don't necessarily provide the highest rise times, but they provide
relatively good rise times given how easy it is to control their overshoot and steady-state error.
The PID controller exhibited the shorter rise time of the two and the better steady state error.
However, the lead-lag provides the benefit of achieving a target steady-state error directly by
parameterization, making it much more trivial to control its steady-state error relative to the PID.
Having said that, note that a lead-lag controller cannot modify the Type of the system beacuse it
does not offer the flexibility of adding pure $s$ factors to the denominator of the open loop
transfer function. This makes it very difficult to achieve zero steady-state error to certain
inputs.
\end{document}
