function y = get_ll(wc)
    phi_m = 60*(pi/180);
    wco=wc;
    
    w=logspace(-6,6,2000);
    plant_sys=nd2sys(3.255,[1,14.15,0]);
    %plantjw=frsp(plant_sys,w);
%    vplot('bode_g',plantjw);
    
    G_jw_co=frsp(plant_sys,wc);
    Gwco=xtracti(vabs(Gjwco),1,1); % plant gain at crossover
    kwco=1/Gwco; % controller gain at crossover
    phGwco=negangle(xtracti(Gjwco,1,1)); %plant phase at crossover
    
    phKwco= -pi + phim-phGwco; %controller phase at crossover
    tau_h = tan(phKwco)/wco; % lead parameter
    k=kwco/sqrt((tau_h*wco)^2+1);
    
    lead=nd2sys([k*tau_h 1],[0.0000001 1]); % lead section of controller
    
    leadloop=mmult(lead,plant_sys); % multiply them
    leadloopjw=frsp(leadloop,w);
    figure(2);
    %vplot('bode_g',leadloopjw);
    t_resp=trsp(
end
