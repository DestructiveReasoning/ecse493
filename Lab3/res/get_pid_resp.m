function sys = get_pid_resp(P,I,D)
    s = tf('s');
    C = P + I/s + s*D;
    G = tf(3.25,[1, 14.15, 0]);
    sys = ((C*G)/(1 + C*G))*0.2;
    step(sys);
    figure();
    bode(sys);
end
