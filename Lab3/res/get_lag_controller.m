function y = get_lag_controller(plant, lead, ws, sensitivity)
    %1/(1 + klag lead(ws)plant(ws)) < sensitivity
    %1 + klag lead(ws)plant(ws) < 1/sensitivity
    %klag lead(ws)plant(ws) < 1/sensitivity - 1
    klag = (1/sensitivity - 1) / norm(freqresp(plant * lead, ws))
    tb = 1/ws;
    b = 1/klag;
    y = klag * tf([(b*tb) 1],[tb 1]);
end
