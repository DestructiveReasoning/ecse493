\contentsline {chapter}{An Excogitating Exordium}{i}{chapter*.1}
\contentsline {part}{I\hspace {1em}Proportional, Integral, and Derivative Control}{1}{part.1}
\contentsline {chapter}{On the Finagling of PID Parameters}{2}{chapter*.6}
\contentsline {chapter}{Designing a PID Controller to Meet Harsh Specifications}{8}{chapter*.13}
\contentsline {part}{II\hspace {1em}Lead-Lag Control}{10}{part.2}
\contentsline {chapter}{On the Design of a Garden-Variety Lead Controller}{11}{chapter*.15}
\contentsline {chapter}{Experimental Performance of the Lead Controller}{15}{chapter*.19}
\contentsline {chapter}{Adding a Lag Compensator to the Lead Controller of Yesteryear}{16}{chapter*.21}
\contentsline {chapter}{Experimental Observations of the Lead-Lag Controller}{20}{chapter*.26}
