pkg load control;

Jm = 0.01;
b = 0.001;
Ke = 0.02;
Kt = 0.02;
Ra = 10;

sys = tf([Kt/Ra], [Jm (b + Kt*Ke/Ra)]);
step(sys)
