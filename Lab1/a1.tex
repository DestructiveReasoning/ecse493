\documentclass[12pt]{article}
\usepackage{amsmath, amssymb, amsfonts}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{bold-extra}
\usepackage{float}
\usepackage{gensymb}
\usepackage{tikz}
\usepackage{color}
\usetikzlibrary{shapes,arrows}
\usepackage[margin=2cm]{geometry}

\definecolor{dgreen}{rgb}{0.4,0.8,0.4}

\hypersetup {
	colorlinks=true,
	linkcolor=blue,
}

\lstset {
	language=Octave,
	breaklines=true,
	basicstyle=\ttfamily\footnotesize,
	showspaces=false,
	frame=single,
}

\tikzstyle{block} = [draw, fill=blue!20, rectangle, minimum height=3em, minimum width=3em]
\tikzstyle{gain} = [draw, fill=blue!20, regular polygon, regular polygon sides=3, shape border
rotate=270,minimum height=3em, minimum width=3em]
\tikzstyle{sum} = [draw, circle, node distance=1cm]
\tikzstyle{input} = [coordinate]
\tikzstyle{output} = [coordinate]

\title{ECSE 493 - Control and Robotics Lab\\Lab Assignment 1}
\author{Harley Wiltzer\\260690006\and Jacob Shnaidman\\260655643}
\date{\today}

\begin{document}
\maketitle
\begin{enumerate}
	\item The equation of motion for a DC motor can be described by:
	\begin{equation}\label{eq:de}
		J_m\ddot{\theta} + \left(b + \frac{K_t}{K_e}\right)\dot{\theta} = \frac{K_t}{R_a}v_a
	\end{equation}
	where:
	\begin{itemize}
		\item $\theta$ is the shaft angle
		\item $J_m = 0.01$kgm$^2$ is the inertia of the rotor and the shaft
		\item $b = 0.001$Nms is the viscous friction coefficient
		\item $K_e = 0.002$Vs is the back emf constant
		\item $K_t = 0.02$Nm/A is the motor torque constant
		\item $R_a = 10\Omega$ is the armature resistance
	\end{itemize}
	\begin{enumerate}
		\item Find the transfer function between the applied voltage and the speed of the motor shaft.\\\\
			\begin{equation*}
				\begin{aligned}
					\mathcal{L}\left(J_m\ddot{\theta} + \left(b + \frac{K_tK_e}{R_a}\right)\dot{\theta}\right) &= \mathcal{L}\left(\frac{K_t}{R_a}v_a\right)\\
					J_m\left(s\dot{\Theta}(s) - \theta(0)\right) + \left(b + \frac{K_tK_e}{R_a}\right)\dot{\Theta}(s) &= \frac{K_t}{R_a}V(s)\\
					\dot{\Theta}(s)\left(sJ_m + \left(b + \frac{K_tK_e}{R_a}\right)\right) &= \frac{K_t}{R_a}V(s) && \mbox{Assuming $\theta(0) = 0$}\\
					\mathcal{H}(s)\triangleq\frac{\dot{\Theta}(s)}{V(s)} &= \frac{K_t}{R_a}\left(\frac{1}{sJ_m + \left(b + \frac{K_tK_e}{R_a}\right)}\right)\\
				\end{aligned}
			\end{equation*}
			Therefore, the transfer function between the applied voltage and the speed of the motor shaft is defined by:
			\begin{equation}\label{eq:tf:1}
				\mathcal{H}(s) = \frac{0.002}{0.01s + 0.00104}
			\end{equation}
		\item Repeat part (a) but use the Matlab command \texttt{tf} and the values above to define a transfer function between the applied voltage and the speed of the motor shaft. Use the step command to estimate the steady-state speed of the motor after a voltage is applied.\\\\
			The transfer function in Eq. (\ref{eq:tf:1}) was implemented in GNU Octave, and subsequently its step response was plotted and observed (see Figure \ref{f:step1}). The code used to generate the plot is shown in Figure \ref{f:code:1b}.\\\\
			\begin{figure}[h]
				\begin{center}
					\caption{Step response of the transfer function in Eq. (\ref{eq:tf:1})}
					\label{f:step1}
					\includegraphics[scale=0.6]{1b}
				\end{center}
			\end{figure}
			\begin{figure}[h]
				\begin{center}
					\caption{Code written to generate plot in Figure \ref{f:step1}}
					\label{f:code:1b}
					\lstinputlisting{1b.m}
				\end{center}
			\end{figure}
			By inspection of Figure \ref{f:step1}, the steady state speed was measured to be $1.924$rad/s.
		\item Use the step response plot to estimate how long it takes the motor to reach within 1\% of the steady state angular velocity.\\\\
			In part (b), the steady state angular velocity was estimated to be $1.924$rad/s. Therefore, the motor reaches within 1\% of the steady state angular velocity when its angular velocity is $0.99(1.924) = 1.9048$rad/s. Investigating the step response plot shows that this takes approximately $45.6$s.
		\item Look up the Final Value Theorem and calculate the steady state speed of the motor after a voltage isapplied. How close was your graphical estimate of the final value?\\\\
			The step response of the system can be expressed as
			\begin{equation}
				\dot{\theta}(t) = u(t)*h(t)\Longleftrightarrow\mathcal{F}(u(t))\mathcal{H}(s) = \frac{1}{s}H(s)
			\end{equation}
			Using the Final Value Theorem, we may compute the steady state angular velocity of the motor as follows:
			\begin{equation*}
				\begin{aligned}
					\lim_{t\to\infty}\dot{\theta}(t) &= \lim_{s\to 0}s\left(\frac{1}{s}\mathcal{H}(s)\right)\\
						&= \lim_{s\to 0}\mathcal{H}(s)\\
						&= \lim_{s\to 0}\frac{0.002}{0.01s + 0.00104} = 1.9231
				\end{aligned}
			\end{equation*}
			The graphical estimate of the final value differs by $0.047\%$.
		\item Find the transfer function between the applied voltage and the shaft angle.\\\\
			We follow a similar process to part (a) below, assuming $\dot\theta(0) = \theta(0) = 0$:
			\begin{equation*}
				\begin{aligned}
					\mathcal{L}\left(J_m\ddot{\theta} + \left(b + \frac{K_tK_e}{R_a}\right)\dot{\theta}\right) &= \mathcal{L}\left(\frac{K_t}{R_a}v_a\right)\\
					J_m\left(s^2\Theta(s) - s\dot{\theta}(0) - \theta(0)\right) + \left(b + \frac{K_tK_e}{R_a}\right)\left(s\Theta(s) - \theta(0)\right) &= \frac{K_t}{R_a}V(s)\\
					\Theta(s)\left(J_ms^2 + \left(b + \frac{K_tK_e}{R_a}\right)s\right) &= \frac{K_t}{R_a}V(s)\\
					\mathcal{H_{\theta}}(s) \triangleq\frac{\Theta(s)}{V(s)} &= \frac{K_t}{R_a}\left(\frac{1}{J_ms^2 + \left(b + \frac{K_tK_e}{R_a}\right)s}\right)
				\end{aligned}
			\end{equation*}
			So, the transfer function from applied voltage to shaft angle is defined by:
			\begin{equation}\label{eq:t:angle}
				\mathcal{H_{\theta}}(s) = \frac{0.002}{0.01s^2 + 0.00104s}
			\end{equation}
		\item Apply feedback to the transfer function in (d). Draw a block diagram of the system.
			What is the transfer function of the feedback system?\\\\
			The block diagram is shown in Figure \ref{f:block-diagram}.
			\begin{figure}[h]
				\caption{Block diagram of the closed-loop system}\label{f:block-diagram}
				\begin{center}
					\begin{tikzpicture}[auto, node distance=2cm, >=latex']
						\node [input, name=input] {};
						\node [sum, right of=input, node distance=1.5cm] (sum) {};
						\node [gain, right of=sum] (k) {$K$};
						\node [block, right of=k, node distance=3cm] (controller)
						{$\mathcal{H}_\theta(s)$};
						\coordinate [below of=controller, node distance=2cm] (space) {};
						\node [output, right of=controller] (output) {};

						\draw [draw, ->] (input) -- node {$\theta_{\text{ref}}$} node [pos=0.95] {$+$} (sum);
						\draw [->] (sum) -- node {$e$} (k);
						\draw [->] (k) -- node {$v_a$} (controller);
						\draw [->] (controller) -- node [name=theta] {$\theta$} (output);
						\draw [-] (theta) |- (space);
						\draw [->] (space) -| node [pos=0.95] {$-$} (sum);
					\end{tikzpicture}
				\end{center}
			\end{figure}
			The closed-loop transfer function $T(s)$ can now be computed:
			\begin{equation*}
				\begin{aligned}
					T(s) &\triangleq\frac{\Theta(s)}{\Theta_\text{ref}}\\
					\Theta(s) &= K(\Theta_\text{ref} - \Theta(s))\mathcal{H}_\theta(s)\\
					\Theta(s)(1 + K\mathcal{H}_\theta(s)) &= K\mathcal{H}_\theta(s)\Theta_\text{ref}\\
					\therefore T(s) &= \frac{K\mathcal{H}_\theta(s)}{1 + K\mathcal{H}_\theta(s)}
				\end{aligned}
			\end{equation*}
		\item What are the units of $K$?\\\\
			The units of $K$ must be $V/\text{rad}$.
		\item What is the input into the motor?\\\\
			The input into the motor is $v_a$, which is given by the following relation:
			\begin{equation}
				v_a = K(\theta_\text{ref}-\theta)
			\end{equation}
		\item Find the transfer function between $v_a$ and $\theta$.\\\\
			The transfer function is that which is shown in Eq. (\ref{eq:t:angle}). However, the
			closed loop transfer function (between $\theta_\text{ref}$ and $\theta$) is derived
			below and given in Eq. (\ref{eq:closed-loop-t}).
		\item What is the maximum value of $K$ that can be used if an overshoot of $M_p<20\%$ is
			desired?\\\\
			First, we shall expand on the transfer function $T(s)$ shown above:
			\begin{equation*}
				\begin{aligned}
					T(s) &= \frac{K\mathcal{H}_\theta(s)}{1 + K\mathcal{H}_\theta(s)}\\
					&= K\left(\frac{0.002}{0.01s^2+0.00104s}\right)\left(1 + K\frac{0.002}{0.01s^2 +
					0.00104s}\right)^{-1}\\
					&= K\left(\frac{0.002}{0.01s^2 + 0.00104s}\right)\left(\frac{0.01s^2 + 0.00104s +
					0.002K}{0.01s^2 + 0.00104s}\right)^{-1}\\
					&= K\left(\frac{0.002}{0.01s^2 + 0.00104s + 0.002K}\right)\\
				\end{aligned}
			\end{equation*}
			Therefore,
			\begin{equation}\label{eq:closed-loop-t}
				T(s) = \frac{0.2K}{s^2 + 0.104s + 0.2K} \triangleq \frac{\omega_n^2}{s^2 +
					2\zeta\omega_ns + \omega_n^2}\\
			\end{equation}
			\begin{equation}\label{eq:omega_n}
					\omega_n^2 = 0.2K\rightarrow\omega_n=\sqrt{0.2K}\\
			\end{equation}
			\begin{equation}\label{eq:zeta}
				2\zeta\omega_n = 0.104\rightarrow\zeta=\frac{0.104}{2\sqrt{0.2K}}
			\end{equation}
			Now, we may use the formula below to determine the maximum overshoot $M_p$ of the
			second-order system:
			\begin{equation}\label{eq:overshoot}
				M_p = 100e^{-\frac{\zeta\pi}{\sqrt{1-\zeta^2}}}\%
			\end{equation}
			Using Eq. (\ref{eq:overshoot}), we find the corresponding $K$ that gives an overshoot of
			$20\%$:
			\begin{equation*}
				\begin{aligned}
					100e^{-\frac{\zeta\pi}{\sqrt{1-\zeta^2}}} &= 20\\
					e^{-\frac{\zeta\pi}{\sqrt{1-\zeta^2}}} &= 0.2\\
					-\frac{\zeta\pi}{\sqrt{1-\zeta^2}} &= \log(0.2) = -1.6094\\
					\frac{\frac{0.104}{2\sqrt{0.2K}}\pi}{\sqrt{1 - \frac{0.104^2}{0.8K}}} &=
					1.6094\\
					\frac{0.104\pi}{2\sqrt{0.2K - \frac{0.010816}{4}}} &= 1.6094\\
					\sqrt{0.2K-0.0027040} &= \frac{0.104\pi}{2(1.6094)}\\
					0.2K - 0.0027040 &= 0.010303\\
					0.2K &= 0.013\\
					K &= 0.065
				\end{aligned}
			\end{equation*}
		\item What value of $K$ provides a rise time of $4$ seconds (ignore overshoot constraint
			above)?\\\\
			To determine the value of $K$ needed for a rise time of $4$ seconds, step responses to
			systems of the form shown in Eq. (\ref{eq:closed-loop-t}) were plotted for various $K$,
			and their rise times were measured as the difference in time between the times when the
			system reached 5\% of its steady state value and when it reached 95\% of its steady
			state value. Figure \ref{f:1k} shows the response that was observed when $K$ was set to
			$0.58$.\\\\
			On the plot shown in Figure \ref{f:1k}, $h(0.95) \approx 0.05$, and $h(4.95)\approx
			0.95$. Therefore, the rise time is approximately $4$ seconds.
			\begin{figure}[H]
				\caption{Step response for the position servo with $K=0.58$}\label{f:1k}
				\begin{center}
					\includegraphics[scale=0.4]{1k}
				\end{center}
			\end{figure}
		\item Plot the step response of the position servo for $K=0.5,1,2$ and use the plots to find
			the rise time and \% overshoot for each value of $K$.\\\\
			Figures \ref{f:1l1}, \ref{f:1l2}, and \ref{f:1l3} show the step responses of the systems
			with $K=0.5,1,2$ respectively.\\\\
			\begin{figure}[H]
				\caption{Step response with $K=0.5$}\label{f:1l1}
				\begin{center}
					\includegraphics[scale=0.5]{1l1}
				\end{center}
			\end{figure}
			\begin{figure}[H]
				\caption{Step response with $K=1$}\label{f:1l2}
				\begin{center}
					\includegraphics[scale=0.5]{1l2}
				\end{center}
			\end{figure}
			\begin{figure}[H]
				\caption{Step response with $K=2$}\label{f:1l3}
				\begin{center}
					\includegraphics[scale=0.5]{1l3}
				\end{center}
			\end{figure}
			The maximum value seen in the plot of the system for $K=0.5$ in Figure \ref{f:1l1} was
			seen to be $1.594$, which
			corresponds to an overshoot of $59.4\%$. Furthermore, values of $0.05$ and $0.95$ were
			seen at times $1.098$s and $5.389$s respectively, which corresponds to a rise time of
			$4.291$s.\\\\
			In the plot of Figure \ref{f:1l2} corresponding to the step response of the system with
			$K=1$, a maximum value of $1.693$ was observed, which means
			that when $K=1$ there is a maximum overshoot of $69.3\%$. On this plot, values of $0.05$
			and $0.95$ were seen at times $0.7721$s and $3.6640$s respectively, therefore the rise
			time is $2.8919$s.\\\\
			Finally, in the plot of Figure \ref{f:1l3} for the step response of the system with
			$K=2$, the maximum observed value was $1.772$, which corresponds to a maximum overshoot
			of $77.2\%$. The, the values $0.05$ and $0.095$ where observed at times $0.5239$s and
			$2.534$s respectively, which yields a rise time of $2.0101$s.
		\item How do the results in (i) and (j) compare with what you found in (k)?\\\\
			In problem (j), we found that the system described by a transfer
			function of the form of Eq. (\ref{eq:closed-loop-t}) with gain
			$K=0.065$ had a maximum overshoot of $20\%$. Then in part (k), we
			found that the system with $K=0.58$ had a maximum overshoot of
			approximately $65\%$. However, although there was more overshoot
			seen in part (k), its rise time (4 seconds) was significantly
			quicker than that in part (j), which was seen to be approximately 16
			seconds.
		\item What feature of the step response is $K$ controlling?\\\\
			As seen in Figures \ref{f:1l1}, \ref{f:1l2}, and \ref{f:1l3}, increasing $K$ tends to
			decrease the rise time of the step response of the system. Moreover, although the rise
			time decreases, the overshoot increases as well. This is because the controller is
			over-compensating for the error in shaft angle (the system is \textit{under-damped}).
			Contrarily, if $K$ was made smaller, the system would under-compensate for error in the
			shaft angle, and the rise time would be far longer. In this scenario, however, no
			overshoot would be observed (the system is \textit{over-damped}).
	\end{enumerate}
	\item Enter the following in Matlab:
		\begin{lstlisting}
p = [1 1];
q = [1 5 6 0];
sys = tf(p, q);
rlocus(sys);
		\end{lstlisting}
		These commands will produce a Root-Locus plot.
		\begin{enumerate}
			\item What are \texttt{p} and \texttt{q}?\\\\
				The variables $p$ and $q$ encode polynomials for the numerator and denominator of a
				transfer function. These variables encode the following:
				\begin{equation*}
					\begin{aligned}
						p &= s + 1\\
						q &= s^3 + 5s^2 + 6s + 0
					\end{aligned}
				\end{equation*}
			\item Explain what a Root Locus plot is. Describe the plot produced by the commands
				above, and explain how the response of the system is represented by the plot.\\\\
				Figure \ref{f:root-locus} shows the Root Locus plot that was generated.
				\begin{figure}[h]
					\begin{center}
						\caption{Generated Root Locus plot}\label{f:root-locus}
						\includegraphics[scale=0.6]{2b}
					\end{center}
				\end{figure}
				A Root Locus plot shows where the open-loop poles and zeroes of
				a system lie on the complex plane, and shows how the locations
				of these poles and zeroes change as the gain of the feedback
				system is modified. By examining the Root Locus plot, one can
				determine if a system can be made stable, since stable systems
				only have in the left have plane ($\Re\{p_i\}<0$ for all poles $p_i$ of the system).
				In Figure \ref{f:root-locus}, we see that no poles are in the right-half plane, but
				there is a pole at the origin. Therefore, this system is marginally stable. However,
				according to the locus at this pole, it can be concluded that by varying the gain,
				the pole can be moved strictly to the left-half plane, so the system can be made
				stable. Moreover, the Root Locus plot shows that the system has poles at $-3,-2,0$
				and a zero at $-1$, which characterize the corner frequencies in the system's
				frequency response.\\\\
				Moreover, since the Root-Locus plot shows locations of poles and
				zeroes, it also provides information about the behavior of the
				system, as it describes the corner frequencies in the system's
				frequency response. The loci demonstrate how these corner frequencies (positions of
				poles and zeroes) change is the gain is varied.
		\end{enumerate}
	\item Write the frequency response transfer function. Use a bode plot to
		analyse the system and design the controller.
		\begin{enumerate}
			\item Determine the proportional controller with maximum margin of
				stability.\\\\
				We obtain the frequency response transfer function by
				substituting $s=j\omega$ into the closed loop transfer
				function from Eq. (\ref{eq:closed-loop-t}):
				\begin{equation}\label{eq:closed-loop-t-f}
					T(j\omega) = \frac{0.2K}{-\omega^2 + 0.104j\omega + 0.2K}
				\end{equation}
				This leads to the bode plot shown in Figure \ref{f:bode-p}.
				\begin{figure}[h]
					\caption{Bode plots for various values of
					$K$}\label{f:bode-p}
					\begin{center}
						\includegraphics[scale=0.6]{3-bodes}
					\end{center}
				\end{figure}
				As seen in Figure \ref{f:bode-p}, there is no phase crossover
				frequency, because the phase never reaches $-\pi$ radians in a
				second order system. Therefore, regardless of $K$, the gain
				margin is infinite. However, the phase margins differ on the
				various plots. For higher values of $K$, we see that the minimum
				phase of each system when gain is $0$dB tends to get closer to
				$-\pi$ radians as $K$ increases. Therefore, systems of the form
				in Eq. (\ref{eq:closed-loop-t}) have larger phase margins when
				$K$ is minimized. So, there is no controller that
				minimizes phase margin because as you decrease $K$, phase margin
				increases monotonically. However, as you decrease $K$, your rise
				time increases as well, which is usually unfavorable. We conclude by selecting the
				controller with $K=0.0625$, as it has the greatest phase margin shown in Figure
				\ref{f:bode-p}, and also has low maximum overshoot (at less than $20\%$). However,
				as discussed previously, this system will have a fairly large rise time, which is
				unfavorable. Since this controller has a phase margin of approximately $80\degree$
				(which is considerably large), we argue that further increasing the phase margin is
				not worth the increase in rise time that it will cause.
			\item Design a lead-lag controller and explain your design.\\\\
				In part (a), it was seen that the proportional controller with $K=4$ had the
				shortest rise time, but also the smallest phase margin. Therefore, we will try to
				implement a lead-lag compensator that increases the phase margin while maintaining a
				short rise time.\\\\
				First, we examine the Bode plot of the open-loop proportional controller with $K=4$
				in Figure \ref{f:bode-k-4}.
				\begin{figure}[h]
					\caption{Bode plot of proportional controller with $K=4$, open
					loop}\label{f:bode-k-4}
					\begin{center}
						\includegraphics[scale=0.5]{bode-k-4}
					\end{center}
				\end{figure}
				We see that the gain is $0$dB at a phase $\omega_c \approx 0.9041$rad. The phase at
				this frequency is approximately $\phi_{\omega_c} = -173.6\degree$. Therefore, it is
				desirable to
				implement a lead controller that will increase the phase at $\omega_c$.\\\\
				We will design a lead controller of the following form:
				\begin{equation}\label{eq:form-lead}
					\mathcal{H}_\text{lead}(s) = \frac{\alpha\tau_as+1}{\tau_as +
					1}\hspace{1cm}\alpha>1
				\end{equation}
				This controller will have a zero at frequency $(\alpha\tau_a)^{-1}$, followed by a
				pole at $\tau_a^{-1}$. Moreover, this controller will demonstrate a increase of
				phase starting a decade before the zero, and phase will return back to $0$ a decade
				after the pole. The frequency for which phase will be at its peak is at the
				geometric mean of the zero frequency and pole frequency:
				\begin{equation}\label{eq:find-ta}
					\omega_\text{max} = \sqrt{\frac{1}{\alpha\tau_a(\tau_a)}} =
					\frac{1}{\tau_a}\sqrt{\frac{1}{\alpha}}
				\end{equation}
				If a phase margin of $\phi$ is desired, the phase shift due to the lead controller
				$\phi_\text{lead}$ can be expressed as:
				\begin{equation}\label{eq:find-lead-phase}
					\phi_\text{lead} = \phi -\phi_{\omega_c}-180\degree
				\end{equation}
				Say a phase margin of $50\degree$ is desired. Then, by Eq.
				(\ref{eq:find-lead-phase}), we have $\phi_{lead} = 43.6\degree$. When a pole is
				places one decade after a zero, the resulting phase peak will be slightly less than
				$45\degree$ by analysis of Bode phase plots. This is a good approximation for the
				desired $43.6\degree$ phase shift. To have the zero placed one decade before the
				pole, we have $\alpha = 10$. Then, given that the phase shift is desired at
				$\omega_\text{max} = \omega_c = 0.9041$rad, we find $\tau_a$ according to Eq.
				(\ref{eq:find-ta}), and observe that $\tau_a = 0.34977$. Therefore, we use the
				$\alpha,\tau_a$ found above to design the lead controller of the form shown in Eq.
				(\ref{eq:form-lead}).\\\\
				The bode plot of the open loop system
				$K\mathcal{H}_\text{lead}(s)\mathcal{H}_\theta(s)$ is shown in Figure
				\ref{f:bode-open-lead}.
				\begin{figure}[h]
					\caption{Open loop Bode plot of Lead Controller followed by Proportional
					Controller}\label{f:bode-open-lead}
					\begin{center}
						\includegraphics[scale=0.5]{bode-open-lead}
					\end{center}
				\end{figure}
				The phase margin is now approximately $48\degree$, which is a vast improvement. The
				closed loop step response of this system is shown in Figure
				\ref{f:closed-step-lead}.
				\begin{figure}[h]
					\caption{Closed loop step response with the Lead
					Controller}\label{f:closed-step-lead}
					\begin{center}
						\includegraphics[scale=0.5]{closed-step-lead}
					\end{center}
				\end{figure}
				Note that the rise time for this system is particularly low, and the settle time is
				far lower than that of the proportional controllers examined earlier. Furthermore,
				the maximum overshoot is less than $24\%$, which is also a vast
				improvement over the high-gain proportional controllers seen previously.\\\\
				Finally, we examine the bode plot of the closed loop system in Figure
				\ref{f:bode-closed-lead}.
				\begin{figure}[h]
					\caption{Bode plot of closed loop Lead Controller
					system}\label{f:bode-closed-lead}
					\begin{center}
						\includegraphics[scale=0.5]{bode-closed-lead}
					\end{center}
				\end{figure}
				We see in the bode plot of Figure \ref{f:bode-closed-lead} that the phase when gain
				is $0$dB is approximately $-100\degree$, corresponding to a phase margin of
				$80\degree$. Again, this is a major improvement over the phase margins found in 3
				(a). The final transfer function for the lead controller is given in Eq.
				(\ref{eq:final:tf}).
				\begin{equation}\label{eq:final:tf}
					T_\text{lead}(s) = 4\left(\frac{3.4977s + 1}{0.34977s + 1}\right)
				\end{equation}
				For closure, we will design a decent lead-lag controller in order to examine its
				performance effects for the step response of the system. Say we wish the reduce the
				gain to $0$dB when frequency is $1$rad/s, in order to reduce high frequency gain and
				make the system less susceptible to instability due to noise. We see in Figure
				\ref{f:bode-open-lead} that the gain at $1$rad/s is approximately $8$dB. Therefore,
				to compensate for this gain with the lag controller, we must have:
				\begin{equation}
					\frac{1}{\beta} = 10^{8.3/20}\rightarrow\beta = 0.38459
				\end{equation}
				Then, we'd like to place the zero of the lag compensator one decade before the zero
				of the lead compensator. This extends the bandwidth of low-frequency gain without
				interfering with the modulation due to the lead compensator. So, we get
				\begin{equation*}
					\begin{aligned}
						\frac{1}{\beta\tau_b} &= \frac{1}{10\alpha\tau_a}\\
						\tau_b = 10\left(\frac{\alpha}{\beta}\right)\tau_a &= 90.946
					\end{aligned}
				\end{equation*}
				So, the complete lead-lag controller has the transfer function:
				\begin{equation}\label{eq:ll}
					T_\text{leadlag}(s) = 4\left(\frac{3.4977s+1}{0.34977s +
					1}\right)\left(\frac{34.977s+1}{90.946s + 1}\right)
				\end{equation}
				Figure \ref{f:bode-closed-ll} shows the bode plot of the closed loop system
				utilizing the lead-lag controller.
				\begin{figure}[h]
					\caption{Bode plot of closed loop Lead-Lag Controller
					system}\label{f:bode-closed-ll}
					\begin{center}
						\includegraphics[scale=0.5]{bode-closed-ll}
					\end{center}
				\end{figure}
				We see that the phase margin of this system is approximately $122\degree$, an
				improvement over the lead controller.\\\\
				Figure \ref{f:comparison} shows a comparison between the proportional controller,
				the lead controller shown in Eq. (\ref{eq:final:tf}), and a lead-lag controller
				shown in Eq. (\ref{eq:ll}). Finally, Table \ref{t:results} gives quantitative
				performance measurements of the three systems.\\
				\begin{table}[h]
					\caption{Quantitative performance results of various controllers}\label{t:results}
					\begin{center}
						\begin{tabular}{|c|c|c|c|c|}%\label{t:results}
							\hline
							\textbf{Controller} & \textbf{Rise Time} & \textbf{Settling Time} &
							\textbf{Phase Margin} & \textbf{Overshoot}\\\hline
							Proportional, $K=4$ & $1.6$s & \color{red}$56.6$s & \color{red}8\degree
							& \color{red}$83.1\%$\\\hline
							Lead & \color{dgreen}$0.79$s & \color{dgreen}$2.13$s & $80\degree$ & $23.8\%$\\\hline
							Lead-Lag & \color{red}$1.61$s & $5.88$s & \color{dgreen}$122\degree$ &
							\color{dgreen}$15.1\%$\\\hline
						\end{tabular}
					\end{center}
				\end{table}
				\begin{figure}[h]
					\caption{Comparison of various controllers}\label{f:comparison}
					\begin{center}
						\includegraphics[scale=1.0]{comparison}
					\end{center}
				\end{figure}
		\end{enumerate}
\end{enumerate}
\end{document}
